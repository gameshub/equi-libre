/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
class Arrow extends Asset {
    constructor(startX, startY, toX, toY, color, arrow = false, cpXY = null, width = 1, dashed = null) {
        super();
        this.startX = startX;
        this.startY = startY;
        this.toX = toX;
        this.toY = toY;
        this.arrow = arrow;
        this.color = color;
        this.cpXY = cpXY;
        this.dashed = dashed;
        this.width = width;
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        let distX = this.toX - this.startX;
        let distY = this.toY - this.startY;
        let normal = [-(distY), distX]

        let hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
        normal[0] /= hypo;
        normal[1] /= hypo;

        //permet de déplacer le vecteur normal plus vers l'arrière de la ligne
        let tangent = [ -normal[1] * 57, normal[0] * 57]
        normal[0] *= 14;
        normal[1] *= 14;

        let propX = this.startX + (distX / hypo * (hypo-39));
        let propY = this.startY + (distY / hypo * (hypo-39));

        let line = new PIXI.Graphics();

        if(this.cpXY == null){
            let endX = this.toX;
            let endY = this.toY;
            if(this.arrow){
                endX = this.startX + (distX / hypo * (hypo - 47));
                endY = this.startY + (distY / hypo * (hypo - 47));
            }
            line.lineStyle(this.width, this.color, 1);
            line.moveTo(this.startX, this.startY);

            //si c'est traitillé il appelle la méthode pour dessinner les traits
            if(this.dashed == null){
                line.lineTo(endX, endY);
            }else{
                this.drawDashLine(line, this.startX, this.startY, endX, endY, this.dashed[0] ,this.dashed[1]);
            }

        }else{
            line
                .lineStyle(this.width, this.color, 1)
                .moveTo(this.startX, this.startY)
                .bezierCurveTo(this.cpXY[0], this.cpXY[1], this.cpXY[2], this.cpXY[3], this.toX, this.toY);

            //Flèche au bout de la flèche ronde
            if(this.arrow){
                normal = [ -(this.toY - this.cpXY[3]), this.toX - this.cpXY[2]]
                hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
                normal[0] /= hypo;
                normal[1] /= hypo;
                tangent = [ -normal[1] * 20, normal[0] * 20]
                normal[0] *= 14;
                normal[1] *= 14;
                propX = this.toX;
                propY = this.toY;
            }
        }

        //enlever this.cpXY == null si on veut ajouter flèche au bout des courbes
        if(this.arrow && this.cpXY == null){
            line
                //triangle bout flèche
                .beginFill(this.color, 1)
                .lineStyle(0, this.color, 1)
                .moveTo(this.toX - normal[0] + tangent[0], this.toY - normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(propX , propY)
                .endFill();
        }
        this.container.addChild(line);
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getFromX(){
        return this.startX;
    }

    setFromX(fromX){
        this.startX = fromX;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    drawDashLine(line, x, y, toX, toY, tailleTrais, tailleEspace) {
        let distX = toX - x;
        let distY = toY - y;
        let hypo = Math.sqrt(distX ** 2 + distY ** 2);

        let pointHypo = tailleTrais;

        let startX = x;
        let startY = y;

        while (pointHypo <= hypo) {
            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo));

            line.lineTo(startX, startY);

            pointHypo += tailleEspace;

            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo));

            line.moveTo(startX, startY);

            pointHypo += tailleTrais;

            //permet de dessiner un trait plus petit si besoin pour bien correspondre à la traille voulue
            if((pointHypo - hypo) > 0 && (pointHypo - tailleTrais) < hypo){
                pointHypo = hypo;
            }
        }
    };
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     * @param {string} alignement du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 16, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

/*    setColor(fgColor, bgColor){
        this.lbl.fill = 0x000000;
        this.graphics.beginFill(0xffffff);

    }*/

    update() {
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
/**
 * @classdesc Asset ButtonGrille
 * @author Arnaud Kolly
 * @version 1.0
 */
class ButtonGrille extends Asset {

    /**
     * Constructeur de l'asset boutonGrille
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, height = 30, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        this.fgcolor = fgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 16, fill: fgColor, align: 'center'})

        this.height = height;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text, this.bgColor, this.fgcolor);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text, bgcolor, fgcolor) {
        this.lbl.text = text;
        this.lbl.style.fill = fgcolor
        this.graphics.clear();
        this.graphics.beginFill(bgcolor);
        this.update();
    }

    update() {
        let buttonWidth = this.getWidth();
        let buttonHeight = this.getHeight();

        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0,0);
        this.lbl.x = this.x - buttonWidth / 2 + 2;
        this.lbl.y = this.y - buttonHeight / 2 + 3;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }

    getHeight() {
        return this.autofit ? (this.height < this.lbl.height + 20 ? this.lbl.height + 20 : this.height) : this.height;
    }
}
class ButtonKolly extends Asset {

    constructor(x, y, text) {
        super();
        this.x = x;
        this.y = y;
        this.text = text;

        this.container = new PIXI.Container();
        this.init();
    }

    init(){
     this.container. removeChildren();
     this.buttonText = new PIXI.Text(this.text,{fontFamily: 'Arial', fontSize: 14, fill: "white", align: 'right'});
     this.buttonText.anchor.set(0.5, 0.5);
     this.buttonText.position.set(40, 25);
     this.buttonEnd = new PIXI.Graphics();
     this.buttonEnd.beginFill(0x0000FF);
     this.buttonEnd.drawRect(0,0,80,50);
     this.buttonEnd.buttonMode = true;
     this.buttonEnd.interactive = true;
     this.container.addChild(this.buttonText);
     this.container.addChild(this.buttonEnd);
    }

    setText(text) {
        this.buttonText.text = text;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
    }

    setX(x) {
        this.x = x;
    }

    getWidth() {
        super.getWidth();
    }

    getHeight() {
        super.getHeight();
    }

    setVisible(visible) {
        super.setVisible(visible);
    }
}
/**
 * @classdesc Asset boutonOnglet
 * @author Arnaud Kolly
 * @version 1.0
 */
class ButtonOnglet extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 16, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = 130;
        let buttonHeight = 30;
        //this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
/**
 * @classdesc Asset ButtonResultat
 * @author Arnaud Kolly
 * @version 1.0
 */
class ButtonResult extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, bgColor, fgColor, autofit = false, height = 30, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {number} la rotation du texte à 180°
        this.lbl.rotation = 1.56;*/

        this.height = height;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
        this.update();
    }

    update() {
        let buttonWidth = this.getWidth();
        let buttonHeight = this.getHeight();
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics];
    }


    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.width + 20 ? this.width + 20 : this.width) : this.width;
    }

    getHeight() {
        return this.autofit ? (this.height < this.height + 20 ? this.height + 20 : this.height) : this.height;
    }
}
class Cell extends Asset {


    constructor(x, y, taille, imgBackground) {
        super();
        this.y = y;
        this.x = x;
        this.taille = taille;
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.init();
    }

    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
class Image extends Asset {


    constructor(x, y, taille, imgBackground, text = '') {
        super();
        this.y = y;
        this.x = x;
        this.taille = taille;
        this.imgBackground = imgBackground;
        this.text = text;

        this.container = new PIXI.Container();

        this.onClick = function () {
            console.log('Replace this action with image.setOnClick')
        };

        this.init();
    }

    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;


        let text = new PIXI.Text(this.text,{fontFamily: "\"Arial\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 600, fontSize: 14, fill : 0x000000, align : 'center'});
        text.x = this.x;
        text.y = this.y;
        text.anchor.x = 0.5;
        text.anchor.y = -3;

        bg.interactive = true;
        bg.buttonMode = true;
        bg.on('pointerdown', function (){
            this.onClick();
        }.bind(this));

        this.container.addChild(bg, text);
    }

    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
/**
 * @classdesc Asset ImageResult
 * @author Arnaud Kolly
 * @version 1.0
 */
class ImageResult extends Asset {
    /**
     * Constructeur de l'asset ImageResult
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {number} taille la taille de l'image
     * @param {String} imgBackground l'image
     */
    constructor(x, y, taille, imgBackground) {
        super();
        /** @type {double} la cordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y */
        this.y = y;
        /** @type {number} la taille de l'image*/
        this.taille = taille;
        /** @type {String} l'image*/
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.init();
    }

    /**
     * Méthode d'initialisation et de mise en place des élements
     */
    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
/**
 * @classdesc Asset ImageTrain
 * @author Arnaud Kolly
 * @version 1.0
 */
class ImageTrain extends Asset {
    /**
     * Constructeur de l'asset ImageTrain
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {number} taille la taille de l'image
     * @param {String} imgBackground l'image
     */
    constructor(x, y, taille, imgBackground) {
        super();
        /** @type {double} la cordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y */
        this.y = y;
        /** @type {number} la taille de l'image*/
        this.taille = taille;
        /** @type {String} l'image*/
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.onClick = function () {
            console.log('Replace this action with image.setOnClick')
        };

        this.init();
    }

    /**
     * Méthode d'initialisation et de mise en place des élements
     */
    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
class Pane extends Asset{

    constructor(x, y) {
        super();
        this.x = x;
        this.y = y;
        this.container = new PIXI.Container();
        this.init();
    }

    init(){
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

    }

    getPixiChildren() {
        super.getPixiChildren();
    }

    getY() {
        super.getY();
    }

    getX() {
        super.getX();
    }

    setY(y) {
        super.setY(y);
    }

    setX(x) {
        super.setX(x);
    }

    getWidth() {
        super.getWidth();
    }

    getHeight() {
        super.getHeight();
    }

    setVisible(visible) {
        super.setVisible(visible);
    }
}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        console.log(this.elements);

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
class Shape extends Asset {


    constructor(x, y, taille, bgImage, name, onMove, onDBClick) {
        super();
        this.x = x;
        this.y = y;
        this.taille = taille;
        this.bgImage = bgImage;
        this.name = name;
        this.onMove = onMove;
        this.onDBClick = onDBClick;

        this.container = new PIXI.Container();

        this.isFirstClick = true;

        this.data = undefined;
        this.shape = undefined;

        this.init();
    }

    init() {
        this.container.removeChildren();

        this.shape = PIXI.Sprite.fromImage(this.bgImage);
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.taille;
        this.shape.height = this.taille;

        this.shape.interactive = true;
        this.shape.on('pointerdown', this.onClick.bind(this));
        this.shape.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));

        this.container.addChild(this.shape);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500);
        } else {
            this.isFirstClick = true;
            if (this.onDBClick) {
                this.onDBClick(this);
            }
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;

    }

    onDragMove() {
        if (this.data) {
            let newPos = this.data.getLocalPosition(this.shape.parent);

            newPos.x = newPos.x < (0+this.taille/2) ? (0+this.taille/2) : newPos.x;
            newPos.x = newPos.x > (600-this.taille/2) ? (600-this.taille/2) : newPos.x;
            newPos.y = newPos.y < (0+this.taille/2) ? (0+this.taille/2) : newPos.y;
            newPos.y = newPos.y > (600-this.taille/2) ? (600-this.taille/2) : newPos.y;

            this.shape.x = newPos.x;
            this.shape.y = newPos.y;

            if (this.onMove) {
                this.onMove(this);
            }
        }
    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
class Table extends Asset {


    constructor(x, y, width, height, rows, cols) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rows = rows;
        this.cols = cols;

        this.container = new PIXI.Container();

        this.init()
    }

    init() {
        let table = '';
        let rows = this.rows;
        let cols = this.cols;
        for (var r = 0; r < rows; r++) {
            table += '<tr>';
            for (var c = 0; c < cols; c++) {
                table += '<td>' + '' + '</td>';
            }
            table += '</tr>';
        }
        document.createElement("<table border=1>" + table + '</table>');
    }

    getPixiChildren() {
        super.getPixiChildren();
    }

    getY() {
        super.getY();
    }

    getX() {
        super.getX();
    }

    setY(y) {
        this.y = y;
        super.setY(y);
    }

    setX(x) {
        super.setX(x);
    }

    getWidth() {
        super.getWidth();
    }

    getHeight() {
        super.getHeight();
    }

    setVisible(visible) {
        super.setVisible(visible);
    }
}
class Text extends Asset {
    constructor(x, y, text) {
        super();
        this.x = x;
        this.y = y;
        this.text = text;
        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Ajouter notre forme sur l'élément graphique
        let text = new PIXI.Text(this.text,{fontFamily: "\"Arial\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 600, fontSize: 24, fill : 0x000000, align : 'center'});
        text.x = this.x;
        text.y = this.y;


        this.placement.addChild(text);

        this.container.addChild(this.placement);
    }

    getPixiChildren() {
        return [this.container];
    }

    getX(){
        return this.x;
    }

    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }

    setY(y){
        this.y = y;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
/**
 * @classdesc Asset Texte
 * @author Arnaud Kolly
 * @version 1.0
 */
class Texte extends Asset {
    /**
     * Constructeur de l'asset Texte
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {String} text le texte
     * @param {number} tailleTexte la taille du texte
     * @param {boolean} gras true si on veut en gras sinon false
     */
    constructor(x, y, text, tailleTexte = 12, gras = false) {
        super();
        /** @type {double} la coordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y*/
        this.y = y;
        /** @type {String} le texte */
        this.text = text;
        /** @type {number} la taille du texte */
        this.tailleTexte = tailleTexte;
        /** @type {boolean} texte en gras (true) */
        this.gras = gras;

        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.display();
    }

    /**
     * Méthode permettant d'initialiser et de placer les élements
     */
    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        let text = null;
        if (this.gras) {
            text = new PIXI.Text(this.text, {
                fontFamily: "\"Arial\", cursive, sans-serif",
                fontVariant: "small-caps",
                fontWeight: 600,
                fontSize: this.tailleTexte,
                fill: 0x000000,
            });
        } else {
            text = new PIXI.Text(this.text, {
                fontFamily: "\"Arial\", cursive, sans-serif",
                fontVariant: "small-caps",
                fontSize: this.tailleTexte,
                fill: 0x000000,
            });
        }

        text.x = this.x;
        text.y = this.y;

        this.placement.addChild(text);
        this.container.addChild(this.placement);
    }

    getPixiChildren() {
        return [this.container];
    }

    getX() {
        return this.x;
    }

    setX(x) {
        this.x = x;
    }

    getY() {
        return this.y;
    }

    setY(y) {
        this.y = y;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
/**
 * @classdesc Asset ToggleButtonImage
 * @author Arnaud Kolly
 * @version 1.0
 */
class ToggleButtonImage extends Asset {

    /**
     * Constructeur de l'asset toggleButtonImage
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {String} label le texte du bouton
     * @param {String} image le nom de l'image
     * @param {boolean} fitToText la taille du bouton en fonction du texte
     * @param {number} width la largeur du bouton
     */
    constructor(x, y, label, image, fitToText = false, width = 150) {
        super();
        /** @type {boolean} la taille du bouton en fonction du texte*/
        this.fitToText = fitToText;
        /** @type la référence au toggle group */
        this.refToggleGroup = null;
        /** @type {number} la largeur du bouton*/
        this.width = width;
        /** @type {number} la hauteur du bouton */
        this.height = 0;
        /** @type les paramètres pour passer au steps suivant par exemple
         * @private*/
        this._data = null;
        /** @type {string} Statut courant du bouton */
        this.currentState = 'inactive';
        /** @type {double} la coordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y */
        this.y = y;
        /** @type {String} le texte du bouton */
        this.text = label;
        /** @type {String} l'image du bouton */
        this.image = image;


        this.lastOnClick = function () {
            this.toggle();
            if (this.refToggleGroup != null) {
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics: new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center'}),
                img: new PIXI.Sprite.fromImage(this.image)

            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics: new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center'}),
                img: new PIXI.Sprite.fromImage(this.image)
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics: new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center'}),
                img: new PIXI.Sprite.fromImage(this.image)
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setObject(this.text, this.image);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setObject(text, image) {
        for (let state of Object.keys(this.states)) {
            this.states[state].lbl.text = text;
            this.states[state].img.imgBackground = image;
        }
        this.update();
    }

    /**
     * Mise à jour de la mise en forme des boutons
     */
    update() {
        for (let state of Object.keys(this.states)) {
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width) : this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if (state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if (state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.x = 0.75;
            this.states[state].lbl.anchor.y = 0.5;
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
            this.states[state].img.x = this.x;
            this.states[state].img.y = this.y;
            this.states[state].img.width = 20;
            this.states[state].img.height = 20;
            this.states[state].img.anchor.x = -2;
            this.states[state].img.anchor.y = 0.5;
        }
    }

    toggle() {
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if (this.refToggleGroup != null) {
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.inactive.img,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.active.img,
            this.states.disabled.graphics,
            this.states.disabled.lbl,
            this.states.disabled.img];
    }

    updateView() {
        for (let state of Object.keys(this.states)) {
            if (state === this.currentState) {
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
                this.states[state].img.visible = true;
            } else {
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
                this.states[state].img.visible = false;
            }
        }
    }

    isActive() {
        return this.currentState === 'active';
    }

    isEnabled() {
        return this.currentState !== 'disabled';
    }

    set data(data) {
        this._data = data;
    }

    get data() {
        return this._data;
    }


    show() {
        this.updateView();
    }

    hide() {
        for (let element of this.getPixiChildren()) {
            element.visible = false;
        }
    }

    disable() {
        this.currentState = 'disabled';
        this.onClick = function () {
        };
        this.updateView();
    }

    enable() {
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref) {
        this.refToggleGroup = ref;
    }

    updateFont(font) {
        for (let state of Object.keys(this.states)) {
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()) {
            element.visible = visible;
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }

    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}
/**
 * @classdesc Définition d'une interface (ihm)
 * @author Arnaud Kolly
 * @version 1.1
 */

/**
 * Couleur pour la catégorie Loisirs
 * @type {number} Couleur
 */
const COLOR_LOISIRS = 13471960;
/**
 * Couleur pour la catégorie Education
 * @type {number} Couleur
 */
const COLOR_EDUCATION = 10135489;
/**
 * Couleur pour la catégorie Alimentation
 * @type {number} Couleur
 */
const COLOR_ALIMENTATION = 15461535;
/**
 * Couleur bleu pour les boutons
 * @type {number} Couleur bleu
 */
const COLOR_BUTTON_BLEU = 32767;
/**
 * Couleur vert pour les boutons
 * @type {number} Couleur vert
 */
const COLOR_BUTTON_VERT = 43520;
/**
 * Couleur noir pour le texte
 * @type {number} Couleur noir
 */
const COLOR_NOIR = 0;
/**
 * Couleur blanc pour le texte
 * @type {number} Couleur blanc
 */
const COLOR_BLANC = 16777215;
/**
 * Longeur du texte dans la grille
 * @type {number} Longeur
 */
const LONGEUR_TEXTE_GRILLE = 38;
/**
 * Font Family Arial
 * @type {string} font style arial
 */
const FONTFAMILY_ARIAL = 'Arial';
/**
 * Alignement à centrer
 * @type {string} Alignement centrer
 */
const ALIGNEMENT_CENTER = 'center';
/**
 * Alignement à gauche
 * @type {string} Alignement gauche
 */
const ALIGNEMENT_LEFT = 'left';
/**
 * Alignement à droite
 * @type {string} Alignement droite
 */
const ALIGNEMENT_RIGHT = 'right';

class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
class Create extends Interface {

    constructor(refGame) {

        super(refGame, "create");

    }

    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic){

    }

}
/**
 * @classdesc IHM du mode explorer
 * @author Arnaud Kolly
 * @version 1.0
 */

class Explore extends Interface {

    /**
     * Constructeur de l'ihm du mode explorer
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "explore");
        this.refGame = refGame;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('startExplore'));


        this.showAll();
        this.init();

    }

    /**
     * Méthode métier pour tous les élements
     */
    showAll() {
        this.buttonEdu = new ButtonOnglet(430, 25, this.refGame.global.resources.getOtherText('catEducation'), COLOR_EDUCATION, COLOR_NOIR);
        var nbActiviteEdu = 3;
        var resultEdu = this.grilleVirtuelle(nbActiviteEdu);
        this.newActEdu = this.activite(resultEdu[0][0], resultEdu[0][1], 'ecole', 'Ecole');
        this.newActEdu1 = this.activite(resultEdu[1][0], resultEdu[1][1], 'prive', 'Cours prive');
        this.newActEdu2 = this.activite(resultEdu[2][0], resultEdu[2][1], 'devoirs', 'Devoirs');

        this.buttonEdu.setOnClick(function () {
            this.newActEdu.setVisible(true);
            this.newActEdu1.setVisible(true);
            this.newActEdu2.setVisible(true);
            this.newActAlim.setVisible(false);
            this.newActAlim1.setVisible(false);
            this.newActAlim2.setVisible(false);
            this.newActAlim3.setVisible(false);
            this.newActLoi.setVisible(false);
            this.newActLoi1.setVisible(false);
            this.newActLoi2.setVisible(false);
            this.newActLoi3.setVisible(false);
            this.newActLoi4.setVisible(false);
            this.newActLoi5.setVisible(false);
            this.newActLoi6.setVisible(false);
        }.bind(this));
        this.newActEdu.setOnClick(function () {
            this.popUp('ecole', this.refGame.global.resources.getOtherText('catEducation').toLowerCase());
        }.bind(this));
        this.newActEdu1.setOnClick(function () {
            this.popUp('prive', this.refGame.global.resources.getOtherText('catEducation').toLowerCase());
        }.bind(this));
        this.newActEdu2.setOnClick(function () {
            this.popUp('devoirs', this.refGame.global.resources.getOtherText('catEducation').toLowerCase());
        }.bind(this));
        this.elements.push(this.buttonEdu, this.newActEdu, this.newActEdu1, this.newActEdu2,);

        this.buttonAlim = new ButtonOnglet(290, 25, this.refGame.global.resources.getOtherText('catAlimentation'), COLOR_ALIMENTATION, COLOR_NOIR);
        var nbActiviteAlim = 4;
        var resultAlim = this.grilleVirtuelle(nbActiviteAlim);
        this.newActAlim = this.activite(resultAlim[0][0], resultAlim[0][1], 'petit-dejeuner', 'Petit-dejeuner');
        this.newActAlim1 = this.activite(resultAlim[1][0], resultAlim[1][1], 'diner', 'Diner');
        this.newActAlim2 = this.activite(resultAlim[2][0], resultAlim[2][1], 'en-cas', 'En-cas');
        this.newActAlim3 = this.activite(resultAlim[3][0], resultAlim[3][1], 'souper', 'Souper');
        this.buttonAlim.setOnClick(function () {
            this.newActAlim.setVisible(true);
            this.newActAlim1.setVisible(true);
            this.newActAlim2.setVisible(true);
            this.newActAlim3.setVisible(true);
            this.newActLoi.setVisible(false);
            this.newActLoi1.setVisible(false);
            this.newActLoi2.setVisible(false);
            this.newActLoi3.setVisible(false);
            this.newActLoi4.setVisible(false);
            this.newActLoi5.setVisible(false);
            this.newActLoi6.setVisible(false);
            this.newActEdu.setVisible(false);
            this.newActEdu1.setVisible(false);
            this.newActEdu2.setVisible(false);
        }.bind(this));
        this.newActAlim.setOnClick(function () {
            this.popUp('petit-dejeuner', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.newActAlim1.setOnClick(function () {
            this.popUp('diner', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.newActAlim2.setOnClick(function () {
            this.popUp('en-cas', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.newActAlim3.setOnClick(function () {
            this.popUp('souper', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.elements.push(this.buttonAlim, this.newActAlim, this.newActAlim1, this.newActAlim2, this.newActAlim3);

        this.buttonLoi = new ButtonOnglet(150, 25, this.refGame.global.resources.getOtherText('catLoisirs'), COLOR_LOISIRS, COLOR_NOIR);
        var nbActiviteLoi = 7;
        var resultLoi = this.grilleVirtuelle(nbActiviteLoi);
        this.newActLoi = this.activite(resultLoi[0][0], resultLoi[0][1], 'ecran', 'Activites sur ecran');
        this.newActLoi1 = this.activite(resultLoi[1][0], resultLoi[1][1], 'lecture', 'Lecture');
        this.newActLoi2 = this.activite(resultLoi[2][0], resultLoi[2][1], 'jeux', 'Jeux');
        this.newActLoi3 = this.activite(resultLoi[3][0], resultLoi[3][1], 'sport', 'Sport');
        this.newActLoi4 = this.activite(resultLoi[4][0], resultLoi[4][1], 'musique', 'Musique');
        this.newActLoi5 = this.activite(resultLoi[5][0], resultLoi[5][1], 'creation', 'Creation');
        this.newActLoi6 = this.activite(resultLoi[6][0], resultLoi[6][1], 'menage', 'Taches menageres');
        this.buttonLoi.setOnClick(function () {
            this.newActLoi.setVisible(true);
            this.newActLoi1.setVisible(true);
            this.newActLoi2.setVisible(true);
            this.newActLoi3.setVisible(true);
            this.newActLoi4.setVisible(true);
            this.newActLoi5.setVisible(true);
            this.newActLoi6.setVisible(true);
            this.newActAlim.setVisible(false);
            this.newActAlim1.setVisible(false);
            this.newActAlim2.setVisible(false);
            this.newActAlim3.setVisible(false);
            this.newActEdu.setVisible(false);
            this.newActEdu1.setVisible(false);
            this.newActEdu2.setVisible(false);
        }.bind(this));
        this.newActLoi.setOnClick(function () {
            this.popUp('ecran', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi1.setOnClick(function () {
            this.popUp('lecture', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi2.setOnClick(function () {
            this.popUp('jeux', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi3.setOnClick(function () {
            this.popUp('sport', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi4.setOnClick(function () {
            this.popUp('musique', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi5.setOnClick(function () {
            this.popUp('creation', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi6.setOnClick(function () {
            this.popUp('menage', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.elements.push(this.buttonLoi, this.newActLoi, this.newActLoi1, this.newActLoi2, this.newActLoi3, this.newActLoi4, this.newActLoi5, this.newActLoi6);


    }

    /**
     * Méthode qui permet de créer un élement Image avec un texte en dessous.
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {String} image nom de l'image
     * @param {String} text texte à afficher en dessous de l'image
     * @returns {HTMLImageElement | Image} l'élement Image avec le texte en dessous
     */
    activite(x, y, image, text) {
        this.imgActivite = this.refGame.global.resources.getImage(image).image.src;
        this.img = new Image(x, y, 100, this.imgActivite, text);
        this.img.setVisible(false);
        return this.img;
    }

    /**
     * Méthode qui permet de rechercher le texte d'une sous catégorie dans le scénario
     * @param name la sous categorie
     * @param categorie la catégorie
     * @returns {String} le texte de la sous-catégorie
     */
    jsonParseTexte(sousCat, categorie) {
        let scenAct = JSON.parse(this.refGame.global.resources.getScenario());
        let texte = scenAct[categorie][sousCat]['text'];
        return texte;
    }

    /**
     * Méthode qui permet de créer la pop-up pour chaque sous-catégorie. La pop-up contient le texte, l'image
     * et un text-to-speech pour la sous-catégorie sélectionnée
     * @param {String} sousCat la sous-catégorie
     * @param {String} categorie la catégorie
     */
    popUp(sousCat, categorie) {
        var imgSousCat = this.refGame.global.resources.getImage(sousCat).image.src;
        Swal.fire({
            title: sousCat.toUpperCase(),
            width: 1000,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px">' + '<img src=' + imgSousCat + ' width="160px" height="160px"></td>' +
                '<td><div id="activites" style="text-align: left">' + this.jsonParseTexte(sousCat, categorie) + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });

    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    refreshFont(isOpenDyslexic) {

    }

    /**
     * Cette function permet d'afficher aléatoirement les sous-catégories sur la page
     * @param nbActivite int pour le nombre d'activités
     * @returns {[]} tableau avec les résultats
     */
    grilleVirtuelle(nbActivite) {
        var curActivite = 0;
        var items = [135, 270, 405, 530];
        var grid = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
        var result = [];
        var i = 0;
        var j = 0;
        for (let k = 0; k < nbActivite; k++) {
            result[k] = new Array(0, 0);
        }
        do {
            i = Math.floor(Math.random() * 4);
            j = Math.floor(Math.random() * 4);
            if (grid[i][j] == 0) {
                result[curActivite][0] = items[i];
                result[curActivite][1] = items[j];
                curActivite++;
                grid[i][j] = 1;
            }
        } while (curActivite < nbActivite);
        return result;
    }

}
class Play extends Interface {

    constructor(refGame) {

        super(refGame, "play");

    }

    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic){

    }

}
/**
 * @classdesc IHM du mode entraîner et évaluer
 * @author Arnaud Kolly
 * @version 1.0
 */

class Train extends Interface {

    /**
     * Constructeur de l'ihm de l'exercice du mode entraîner et évaluer
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "train");
        this.refGame = refGame;
        this.generateGame();
    }

    /**
     * Point d'entrée pour l'affichage
     * @param {boolean} evaluate la variable qui permet de définir si le mode évaluer est sélectionner
     */
    show(evaluate) {
        this.clear();
        this.evaluate = evaluate;
        this.setupElements();

        this.setElements(this.extractElements());

        this.updateStepView();

        this.refGame.showText("");


        this.init();
        this.refreshLang(this.refGame.global.resources.getLanguage());
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        if (this.refGame.global.resources.getExercice() != undefined) {
            this.exercices = JSON.parse(this.refGame.global.resources.getExercice().exercice);
            this.exercicesFinal = this.exercices['planning'];

            for (let i = 0; i < this.exercicesFinal.length; i++) {
                if (this.exercicesFinal[i] != "") {
                    var resultScenario = this.getScenario(this.exercicesFinal[i]);
                    var texte = "" + resultScenario["info"];
                    var color = resultScenario["color"];
                    if (texte != 'undefined') {
                        var end
                        if (texte.length > LONGEUR_TEXTE_GRILLE) {
                            var final = texte.slice(0, LONGEUR_TEXTE_GRILLE);
                            end = final + "...";
                        } else {
                            end = texte;
                        }
                        var result = new Object();
                        result.texteFinal = end;
                        result.colorFinal = color;
                        this.exercicesFinal[i] = result;
                    }
                } else {
                    var result = new Object();
                    result.texteFinal = "";
                    result.colorFinal = COLOR_BUTTON_BLEU;
                    this.exercicesFinal[i] = result;
                }
            }

            this.steps.specific[4].button.setText(this.exercicesFinal[0].texteFinal, this.exercicesFinal[0].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button1.setText(this.exercicesFinal[1].texteFinal, this.exercicesFinal[1].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button2.setText(this.exercicesFinal[2].texteFinal, this.exercicesFinal[2].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button3.setText(this.exercicesFinal[3].texteFinal, this.exercicesFinal[3].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button4.setText(this.exercicesFinal[4].texteFinal, this.exercicesFinal[4].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button5.setText(this.exercicesFinal[5].texteFinal, this.exercicesFinal[5].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button6.setText(this.exercicesFinal[6].texteFinal, this.exercicesFinal[6].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button7.setText(this.exercicesFinal[7].texteFinal, this.exercicesFinal[7].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button8.setText(this.exercicesFinal[8].texteFinal, this.exercicesFinal[8].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button9.setText(this.exercicesFinal[9].texteFinal, this.exercicesFinal[9].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button10.setText(this.exercicesFinal[10].texteFinal, this.exercicesFinal[10].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button11.setText(this.exercicesFinal[11].texteFinal, this.exercicesFinal[11].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button12.setText(this.exercicesFinal[12].texteFinal, this.exercicesFinal[12].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button13.setText(this.exercicesFinal[13].texteFinal, this.exercicesFinal[13].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button14.setText(this.exercicesFinal[14].texteFinal, this.exercicesFinal[14].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button15.setText(this.exercicesFinal[15].texteFinal, this.exercicesFinal[15].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button16.setText(this.exercicesFinal[16].texteFinal, this.exercicesFinal[16].colorFinal, COLOR_NOIR);
        }

        for (let alert of Object.keys(this.alert))
            this.alert[alert] = this.refGame.global.resources.getOtherText(alert);

        for (let i = 0; i < this.steps.specific.length; i++) {
            if (i != 4) {
                this.steps.specific[i]["title".concat(i)].text = this.refGame.global.resources.getOtherText('step' + i + 'Title');
                this.steps.specific[i]["subtitle".concat(i)].text = this.refGame.global.resources.getOtherText('step' + i + 'Subtitle');

                let subtitle = this.steps.specific[i]["subtitle".concat(i)].text;
                this.lang.push(this.steps.specific[i]["title".concat(i)].text + ".<br>" + ((typeof subtitle !== 'undefined') ? subtitle : ''));
                //Update size;
                this.steps.specific[i]["subtitle".concat(i)].y = 85 + this.steps.specific[i]["subtitle".concat(i)].height / 2;

            }
        }


        this.steps.specific[0].btnNext0.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[1].btnBack1.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[1].btnNext1.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[2].btnBack2.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[2].btnNext2.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[3].btnBack3.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[3].btnNext3.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[4].btnNext4.setText(this.refGame.global.resources.getOtherText('btnSaveAndNext'));
        this.steps.specific[4].btnBack4.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[5].btnNewExercice.setText(this.refGame.global.resources.getOtherText('btnNewExercice'));
        this.steps.specific[5].btnBack5.setText(this.refGame.global.resources.getOtherText('btnBack'));

        this.steps.general.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: this.refGame.global.resources.getOtherText('mode'),
            harmos: this.refGame.global.resources.getDegre(),
            lang: lang
        });

        this.steps.specific[1].question1.getButtons()[0].setText(this.refGame.global.resources.getOtherText('heure1'));
        this.steps.specific[1].question1.getButtons()[1].setText(this.refGame.global.resources.getOtherText('heure2'));
        this.steps.specific[1].question1.getButtons()[2].setText(this.refGame.global.resources.getOtherText('heure3'));
        this.steps.specific[1].question1.getButtons()[3].setText(this.refGame.global.resources.getOtherText('heure4'));
        this.steps.specific[1].question1.getButtons()[4].setText(this.refGame.global.resources.getOtherText('heure5'));
        this.steps.specific[1].question1.getButtons()[5].setText(this.refGame.global.resources.getOtherText('heure6'));
        this.steps.specific[1].question1.getButtons()[6].setText(this.refGame.global.resources.getOtherText('heure7'));
        this.steps.specific[1].question1.getButtons()[7].setText(this.refGame.global.resources.getOtherText('heure8'));
        this.steps.specific[1].question1.getButtons()[8].setText(this.refGame.global.resources.getOtherText('heure9'));
        this.steps.specific[1].question1.getButtons()[9].setText(this.refGame.global.resources.getOtherText('heure10'));


        this.steps.specific[2].question2.getButtons()[0].setText(this.refGame.global.resources.getOtherText('dent1'));
        this.steps.specific[2].question2.getButtons()[1].setText(this.refGame.global.resources.getOtherText('dent2'));
        this.steps.specific[2].question2.getButtons()[2].setText(this.refGame.global.resources.getOtherText('dent3'));
        this.steps.specific[2].question2.getButtons()[3].setText(this.refGame.global.resources.getOtherText('dent4'));
        this.steps.specific[2].question2.getButtons()[4].setText(this.refGame.global.resources.getOtherText('dent5'));
        this.steps.specific[2].question2.getButtons()[5].setText(this.refGame.global.resources.getOtherText('dent6'));

        this.steps.specific[3].texte.text = this.refGame.global.resources.getOtherText('depTexte2');

        this.steps.specific[3].question3.getButtons()[0].setObject(this.refGame.global.resources.getOtherText('train'));
        this.steps.specific[3].question3.getButtons()[1].setObject(this.refGame.global.resources.getOtherText('bus'));
        this.steps.specific[3].question3.getButtons()[2].setObject(this.refGame.global.resources.getOtherText('voiture'));
        this.steps.specific[3].question3.getButtons()[3].setObject(this.refGame.global.resources.getOtherText('velo'));
        this.steps.specific[3].question3.getButtons()[4].setObject(this.refGame.global.resources.getOtherText('trottinette'));
        this.steps.specific[3].question3.getButtons()[5].setObject(this.refGame.global.resources.getOtherText('pied'));

        this.steps.specific[3].questionTrain.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionTrain.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionTrain.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionTrain.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionBus.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionBus.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionBus.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionBus.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionVoiture.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionVoiture.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionVoiture.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionVoiture.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionVelo.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionVelo.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionVelo.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionVelo.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionTrottinette.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionTrottinette.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionTrottinette.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionTrottinette.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionPied.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionPied.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionPied.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionPied.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[4].label0.text = this.refGame.global.resources.getOtherText("tabLever");
        this.steps.specific[4].label1.text = this.refGame.global.resources.getOtherText("tabMatinee");
        this.steps.specific[4].label2.text = this.refGame.global.resources.getOtherText("tabMidi");
        this.steps.specific[4].label3.text = this.refGame.global.resources.getOtherText("tabApm");
        this.steps.specific[4].label4.text = this.refGame.global.resources.getOtherText("tabSoir");

        this.steps.specific[5].txtAlim.setVisible(false);

    }

    generateGame() {

        this.currentStep = 0;

        this.labelStyle = new PIXI.TextStyle();
        this.titleStyle = new PIXI.TextStyle();
        this.subtitleStyle = new PIXI.TextStyle();


        this.titleStyle.align = ALIGNEMENT_CENTER;
        this.titleStyle.fill = COLOR_NOIR;
        this.titleStyle.fontFamily = FONTFAMILY_ARIAL;
        this.titleStyle.fontSize = 32;
        this.subtitleStyle.align = ALIGNEMENT_CENTER;
        this.subtitleStyle.fill = COLOR_NOIR;
        this.subtitleStyle.fontFamily = FONTFAMILY_ARIAL;
        this.subtitleStyle.fontSize = 18;
        this.subtitleStyle.wordWrap = true;
        this.subtitleStyle.wordWrapWidth = 550;
        this.labelStyle.fontFamily = FONTFAMILY_ARIAL;
        this.labelStyle.fontSize = 22;
        this.labelStyle.align = ALIGNEMENT_LEFT;
        this.labelStyle.fill = COLOR_NOIR;

        /**
         * Variable contenant tous les messages d'alerte.
         * @type {{noType: string, confirmDeletionText: string, noNameText: string, checkConditionText: string, checkCondition: string, noName: string, confirmDeletion: string, noTypeText: string}}
         */
        this.alert = {
            noHeure: '',
            noHeureText: '',
            noDent: '',
            noDentText: '',
            noDeplacement: '',
            noDeplacementText: '',
            noNbDeplacement: '',
            noNbDeplacementText: ''
        };

        /**
         *
         * @type {{general: {}, specific: *[]}}} Variable contenant tous les éléments graphiques pour les différentes interfaces.
         */
        this.steps = {
            specific: [
                // 0 Accueil
                {
                    btnNext0: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    title0: new PIXI.Text('', this.titleStyle),
                    subtitle0: new PIXI.Text('', this.subtitleStyle),
                },
                // 1 Heures de sommeil
                {

                    btnBack1: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    btnNext1: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, true),
                    image1: new ImageTrain(300, 450, 150, this.refGame.global.resources.getImage('sommeil').image.src),
                    title1: new PIXI.Text('', this.titleStyle),
                    subtitle1: new PIXI.Text('', this.subtitleStyle),
                    question1: new ToggleGroup('single', [
                        new ToggleButton(200, 160, '', false),
                        new ToggleButton(200, 200, '', false),
                        new ToggleButton(200, 240, '', false),
                        new ToggleButton(200, 280, '', false),
                        new ToggleButton(200, 320, '', false),
                        new ToggleButton(400, 160, '', false),
                        new ToggleButton(400, 200, '', false),
                        new ToggleButton(400, 240, '', false),
                        new ToggleButton(400, 280, '', false),
                        new ToggleButton(400, 320, '', false)
                    ])
                },
                // 2 = Brossage de dent
                {
                    btnBack2: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    btnNext2: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    image2: new ImageTrain(300, 450, 150, this.refGame.global.resources.getImage('dent').image.src),
                    title2: new PIXI.Text('', this.titleStyle),
                    subtitle2: new PIXI.Text('', this.subtitleStyle),
                    question2: new ToggleGroup('single', [
                        new ToggleButton(200, 160, '', false),
                        new ToggleButton(200, 200, '', false),
                        new ToggleButton(200, 240, '', false),
                        new ToggleButton(400, 160, '', false),
                        new ToggleButton(400, 200, '', false),
                        new ToggleButton(400, 240, '', false)
                    ])
                },
                // 3 = Déplacement
                {
                    btnBack3: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    btnNext3: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    title3: new PIXI.Text('', this.titleStyle),
                    subtitle3: new PIXI.Text('', this.subtitleStyle),
                    texte: new PIXI.Text('', this.subtitleStyle),
                    question3: new ToggleGroup('single', [
                        new ToggleButtonImage(200, 150, '', this.refGame.global.resources.getImage('train').image.src, false),
                        new ToggleButtonImage(200, 190, '', this.refGame.global.resources.getImage('bus').image.src, false),
                        new ToggleButtonImage(200, 230, '', this.refGame.global.resources.getImage('voiture').image.src, false),
                        new ToggleButtonImage(400, 150, '', this.refGame.global.resources.getImage('velo').image.src, false),
                        new ToggleButtonImage(400, 190, '', this.refGame.global.resources.getImage('trottinette').image.src, false),
                        new ToggleButtonImage(400, 230, '', this.refGame.global.resources.getImage('pied').image.src, false)
                    ]),
                    imageTrain: new ImageTrain(250, 340, 50, this.refGame.global.resources.getImage('train').image.src),
                    imageBus: new ImageTrain(250, 420, 50, this.refGame.global.resources.getImage('bus').image.src),
                    imageVoiture: new ImageTrain(250, 500, 50, this.refGame.global.resources.getImage('voiture').image.src),
                    imageVelo: new ImageTrain(500, 340, 50, this.refGame.global.resources.getImage('velo').image.src),
                    imageTrottinette: new ImageTrain(500, 420, 50, this.refGame.global.resources.getImage('trottinette').image.src),
                    imagePied: new ImageTrain(500, 500, 50, this.refGame.global.resources.getImage('pied').image.src),
                    questionTrain: new ToggleGroup('single', [
                        new ToggleButton(80, 340, '', true),
                        new ToggleButton(120, 340, '', true),
                        new ToggleButton(160, 340, '', true),
                        new ToggleButton(200, 340, '', true)
                    ]),
                    questionBus: new ToggleGroup('single', [
                        new ToggleButton(80, 420, '', true),
                        new ToggleButton(120, 420, '', true),
                        new ToggleButton(160, 420, '', true),
                        new ToggleButton(200, 420, '', true)
                    ]),
                    questionVoiture: new ToggleGroup('single', [
                        new ToggleButton(80, 500, '', true),
                        new ToggleButton(120, 500, '', true),
                        new ToggleButton(160, 500, '', true),
                        new ToggleButton(200, 500, '', true)
                    ]),
                    questionVelo: new ToggleGroup('single', [
                        new ToggleButton(335, 340, '', true),
                        new ToggleButton(375, 340, '', true),
                        new ToggleButton(415, 340, '', true),
                        new ToggleButton(455, 340, '', true)
                    ]),
                    questionTrottinette: new ToggleGroup('single', [
                        new ToggleButton(335, 420, '', true),
                        new ToggleButton(375, 420, '', true),
                        new ToggleButton(415, 420, '', true),
                        new ToggleButton(455, 420, '', true)
                    ]),
                    questionPied: new ToggleGroup('single', [
                        new ToggleButton(335, 500, '', true),
                        new ToggleButton(375, 500, '', true),
                        new ToggleButton(415, 500, '', true),
                        new ToggleButton(455, 500, '', true)
                    ]),
                },
                // 4 = Grille exercice
                {
                    title4: new PIXI.Text('', this.titleStyle),
                    subtitle4: new PIXI.Text('', this.subtitleStyle),
                    btnBack4: new Button(525, 520, '', COLOR_BUTTON_VERT, COLOR_BLANC, false, 100),
                    btnNext4: new Button(525, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, true, 100),
                    //Objets pour les colonnes et contour
                    arrow: new Arrow(1, 0, 1, 600, COLOR_NOIR),
                    arrow1: new Arrow(150, 0, 150, 600, COLOR_NOIR),
                    arrow2: new Arrow(449, 0, 449, 600, COLOR_NOIR),
                    arrow3: new Arrow(1, 1, 449, 1, COLOR_NOIR),
                    arrow4: new Arrow(1, 599, 449, 599, COLOR_NOIR),
                    //Objets pour les lignes de separation
                    arrow5: new Arrow(1, 105.9, 449, 105.9, COLOR_NOIR),
                    arrow6: new Arrow(1, 247.1, 449, 247.1, COLOR_NOIR),
                    arrow7: new Arrow(1, 353, 449, 353, COLOR_NOIR),
                    arrow8: new Arrow(1, 494.2, 449, 494.2, COLOR_NOIR),

                    //Objets pour les lignes par categories
                    arrow9: new Arrow(150, 35.3, 449, 35.3, COLOR_NOIR),
                    arrow10: new Arrow(150, 70.6, 449, 70.6, COLOR_NOIR),
                    arrow11: new Arrow(150, 141.2, 449, 141.2, COLOR_NOIR),
                    arrow12: new Arrow(150, 176.5, 449, 176.5, COLOR_NOIR),
                    arrow13: new Arrow(150, 211.8, 449, 211.8, COLOR_NOIR),
                    arrow14: new Arrow(150, 282.4, 449, 282.4, COLOR_NOIR),
                    arrow15: new Arrow(150, 317.7, 449, 317.7, COLOR_NOIR),

                    arrow16: new Arrow(150, 388.3, 449, 388.3, COLOR_NOIR),
                    arrow17: new Arrow(150, 423.6, 449, 423.6, COLOR_NOIR),
                    arrow18: new Arrow(150, 458.9, 449, 458.9, COLOR_NOIR),
                    arrow19: new Arrow(150, 529.5, 449, 529.5, COLOR_NOIR),
                    arrow20: new Arrow(150, 564.8, 449, 564.8, COLOR_NOIR),

                    label0: new PIXI.Text('', this.labelStyle),
                    label1: new PIXI.Text('', this.labelStyle),
                    label2: new PIXI.Text('', this.labelStyle),
                    label3: new PIXI.Text('', this.labelStyle),
                    label4: new PIXI.Text('', this.labelStyle),

                    //Buttons dans la grille
                    button: new ButtonGrille(300, 18, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button1: new ButtonGrille(300, 53, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button2: new ButtonGrille(300, 88, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button3: new ButtonGrille(300, 123, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button4: new ButtonGrille(300, 159, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button5: new ButtonGrille(300, 194, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button6: new ButtonGrille(300, 229, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button7: new ButtonGrille(300, 265, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button8: new ButtonGrille(300, 300, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button9: new ButtonGrille(300, 335, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button10: new ButtonGrille(300, 370, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button11: new ButtonGrille(300, 406, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button12: new ButtonGrille(300, 442, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button13: new ButtonGrille(300, 476, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button14: new ButtonGrille(300, 512, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button15: new ButtonGrille(300, 546, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button16: new ButtonGrille(300, 582, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),

                },
                // 5 = Résultat
                {
                    title5: new PIXI.Text('', this.titleStyle),
                    subtitle5: new PIXI.Text('', this.subtitleStyle),
                    btnBack5: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, false, 100),
                    btnNewExercice: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, true, 100),
                    arrowRes: new Arrow(100, 100, 100, 400),
                    arrowRes1: new Arrow(100, 100, 90, 125),
                    arrowRes2: new Arrow(100, 100, 110, 125),
                    arrowRes3: new Arrow(100, 400, 500, 400),
                    imageOk: new ImageResult(50, 130, 70, this.refGame.global.resources.getImage('camera').image.src),
                    imageMi: new ImageResult(50, 250, 70, this.refGame.global.resources.getImage('camera').image.src),
                    imageKo: new ImageResult(50, 370, 70, this.refGame.global.resources.getImage('camera').image.src),
                    btnResultAlim: new ButtonResult(150, 250, 255, 0Xffffff, false, 300, 60),
                    resultAlimTexte: new Texte(112, 405, 'Alimentation'),
                    txtAlim: new Texte(0, 450, 'Bravo ! Tu as atteint le nombre maximal de points pour la categorie Alimentation.', 14)

                }
            ],
            general: {
                info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: COLOR_NOIR, align: 'left'})
            }
        }
        ;

        this.lang = [];
    }

    /**
     * Paramétrage de la mise en page et des actions sur les différents boutons
     */
    setupElements() {
        /////////////////////////////////////
        //              General            //
        /////////////////////////////////////

        this.steps.general.info.x = 10;
        this.steps.general.info.y = 10;
        this.steps.general.info.anchor.set(0.5);
        this.steps.general.info.visible = false;

        /**
         *
         * @type {(ButtonGrille|ButtonGrille)[]} Tableau des boutons de la grille.
         */
        this.buttons = [
            this.steps.specific[4].button,
            this.steps.specific[4].button1,
            this.steps.specific[4].button2,
            this.steps.specific[4].button3,
            this.steps.specific[4].button4,
            this.steps.specific[4].button5,
            this.steps.specific[4].button6,
            this.steps.specific[4].button7,
            this.steps.specific[4].button8,
            this.steps.specific[4].button9,
            this.steps.specific[4].button10,
            this.steps.specific[4].button11,
            this.steps.specific[4].button12,
            this.steps.specific[4].button13,
            this.steps.specific[4].button14,
            this.steps.specific[4].button15,
            this.steps.specific[4].button16
        ];

        this.steps.specific[0].btnNext0.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnBack1.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnNext1.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[2].btnBack2.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[2].btnNext2.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnBack3.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnNext3.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnBack4.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnNext4.setOnClick(function () {
            console.log("VALIDER")
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].button.setOnClick(function () {
            this.clic = this.buttons[0];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button1.setOnClick(function () {
            this.clic = this.buttons[1];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button2.setOnClick(function () {
            this.clic = this.buttons[2];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button3.setOnClick(function () {
            this.clic = this.buttons[3];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button4.setOnClick(function () {
            this.clic = this.buttons[4];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button5.setOnClick(function () {
            this.clic = this.buttons[5];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button6.setOnClick(function () {
            this.clic = this.buttons[6];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button7.setOnClick(function () {
            this.clic = this.buttons[7];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button8.setOnClick(function () {
            this.clic = this.buttons[8];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button9.setOnClick(function () {
            this.clic = this.buttons[9];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button10.setOnClick(function () {
            this.clic = this.buttons[10];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button11.setOnClick(function () {
            this.clic = this.buttons[11];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button12.setOnClick(function () {
            this.clic = this.buttons[12];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button13.setOnClick(function () {
            this.clic = this.buttons[13];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button14.setOnClick(function () {
            this.clic = this.buttons[14];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button15.setOnClick(function () {
            this.clic = this.buttons[15];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button16.setOnClick(function () {
            this.clic = this.buttons[16];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[5].btnBack5.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[5].btnNewExercice.setOnClick(function () {
            console.log("btnNewExercice");
        }.bind(this));

        for (let i = 0; i < this.steps.specific.length; i++) {
            this.steps.specific[i]["title".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["subtitle".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["title".concat(i)].x = 300;
            this.steps.specific[i]["title".concat(i)].y = 50;
            this.steps.specific[i]["subtitle".concat(i)].x = 300;
            this.steps.specific[i]["subtitle".concat(i)].y = 85;
        }


        /////////////////////////////////////
        //              Step 0             //
        /////////////////////////////////////

        for (let potentialForm of Object.keys(this.steps.specific[0])) {
            if (potentialForm.startsWith("form"))
                this.steps.specific[0][potentialForm].visible = false;
        }

        /////////////////////////////////////
        //              Step 1             //
        /////////////////////////////////////

        this.steps.specific[1].question1.getButtons()[0].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[1].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[2].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[3].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[4].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[5].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[6].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[7].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[8].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[9].data = {
            nextStep: 2
        };

        /////////////////////////////////////
        //              Step 2             //
        /////////////////////////////////////

        this.steps.specific[2].question2.getButtons()[0].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[1].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[2].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[3].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[4].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[5].data = {
            nextStep: 3
        };

        /////////////////////////////////////
        //              Step 3             //
        /////////////////////////////////////

        this.steps.specific[3].question3.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[4].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[5].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].texte.anchor.set(0.5);
        this.steps.specific[3].texte.x = 300;
        this.steps.specific[3].texte.y = 285;

        /////////////////////////////////////
        //              Step 4             //
        /////////////////////////////////////

        this.steps.specific[4].label0.x = 50;
        this.steps.specific[4].label0.y = 35;
        this.steps.specific[4].label1.x = 40;
        this.steps.specific[4].label1.y = 160;
        this.steps.specific[4].label2.x = 60;
        this.steps.specific[4].label2.y = 285;
        this.steps.specific[4].label3.x = 25;
        this.steps.specific[4].label3.y = 410;
        this.steps.specific[4].label4.x = 60;
        this.steps.specific[4].label4.y = 535;

        /////////////////////////////////////
        //              Step 5             //
        /////////////////////////////////////


        this.steps.specific[5].btnResultAlim.setOnClick(function () {
            this.steps.specific[5].txtAlim.setVisible(true);
        }.bind(this));

    }

    /**
     * Méthode retournant tous les éléments graphiques
     * @param {boolean} onlySteps Choix de prendre uniquement les éléments correspondant à une étape spécifique
     * @return {{}} JSON des éléments
     */
    extractElements(onlySteps = false) {
        let elements = {};
        if (!onlySteps)
            for (let element of Object.keys(this.steps.general))
                elements[element] = this.steps.general[element];
        for (let step of this.steps.specific)
            for (let element of Object.keys(step))
                elements[element] = step[element];
        return elements;
    }

    /**
     * Mise à jour de l'interface en fonction de l'étape.
     */
    updateStepView() {
        this.hideAll();
        for (let elementToDisplay of Object.keys(this.steps.specific[this.currentStep])) {
            if (this.currentStep === 0 &&
                (elementToDisplay.startsWith("form") || elementToDisplay.startsWith("btnHideShapes")))
                continue;
            if (this.steps.specific[this.currentStep][elementToDisplay] instanceof Button)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ImageTrain)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ImageResult)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleButton
                || this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleButtonImage
                || this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleGroup)
                this.steps.specific[this.currentStep][elementToDisplay].show();
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof CheckBox)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof Arrow)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ButtonGrille)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ButtonResult)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            // Affichage des noms de catégorie mais pas des texte de résultat
            else if (this.currentStep == 5) {
                this.steps.specific[this.currentStep].resultAlimTexte.setVisible(true);
                this.steps.specific[this.currentStep].txtAlim.setVisible(false);
            } else {
                this.steps.specific[this.currentStep][elementToDisplay].visible = true;
            }
        }
        if (this.currentStep == 4) {
            this.refGame.showText(this.refGame.global.resources.getOtherText('step4Title'));
        } else {
            this.refGame.showText(this.lang[this.currentStep]);
        }
    }

    /**
     * Cache tous les éléments présents sur l'interface
     */
    hideAll() {
        let elements = this.extractElements(true);
        for (let elementName of Object.keys(elements))
            if (elements[elementName] instanceof Button)
                this.setButtonVisible(elements[elementName], false);
            else if (elements[elementName] instanceof ImageTrain)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof ImageResult)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof ToggleButton || elements[elementName] instanceof ToggleButtonImage || elements[elementName] instanceof ToggleGroup)
                elements[elementName].hide();
            else if (elements[elementName] instanceof CheckBox)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof Arrow)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof Texte)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof ButtonResult)
                this.setButtonVisible(elements[elementName], false);
            else if (elements[elementName] instanceof ButtonGrille)
                this.setButtonVisible(elements[elementName], false);
            else
                elements[elementName].visible = false;
    }

    /**
     * Affichage / Désaffichage d'un bouton.
     * @param {Button} button Bouton à modifier
     * @param {boolean} visible Le bouton doit-il être visible
     */
    setButtonVisible(button, visible) {
        for (let pixiElement of button.getPixiChildren()) {
            pixiElement.visible = visible;
        }
    }

    /**
     * Choix de l'étape suivante en fonction de l'étape actuelle;
     */
    chooseNextStep() {
        switch (this.currentStep) {
            case 1:
                if (this.steps.specific[1].question1.getActives().length > 0) {
                    this.currentStep = this.steps.specific[1].question1.getActives()[0].data.nextStep;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noHeureText, this.alert.noHeure);
                break;
            case 2:
                if (this.steps.specific[2].question2.getActives().length > 0) {
                    this.currentStep = this.steps.specific[2].question2.getActives()[0].data.nextStep;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noDentText, this.alert.noDent);
                break;
            case 3:
                if (this.steps.specific[3].question3.getActives().length > 0) {
                    if (this.steps.specific[3].questionTrain.getActives().length > 0 ||
                        this.steps.specific[3].questionBus.getActives().length > 0 ||
                        this.steps.specific[3].questionVoiture.getActives().length > 0 ||
                        this.steps.specific[3].questionVelo.getActives().length > 0 ||
                        this.steps.specific[3].questionTrottinette.getActives().length > 0 ||
                        this.steps.specific[3].questionPied.getActives().length > 0) {
                        this.currentStep++;
                    } else {
                        Util.getInstance().showAlert('warning', this.alert.noNbDeplacementText, this.alert.noNbDeplacement);
                    }
                } else {
                    Util.getInstance().showAlert('warning', this.alert.noDeplacementText, this.alert.noDeplacement);
                }
                break;
            case 4:
                this.currentStep++;
                break;
            case 5:
                this.currentStep++;
                break;
            default :
                this.currentStep = this.currentStep < 10 ? this.currentStep + 1 : 10;
                break;
        }
    }

    /**
     * Choix de l'étape précédante en fonction de l'étape actuelle.
     */
    chooseBackStep() {
        switch (this.currentStep) {
            case 1:
                this.currentStep = 0;
                break;
            case 2:
                this.currentStep = 1;
                break;
            case 3:
                this.currentStep = 2;
                break;
            case 4:
                this.currentStep = 3;
                break;
            case 5:
                this.currentStep = 4;
                break;
            default :
                this.currentStep = this.currentStep > 0 ? this.currentStep - 1 : 0;
                break;
        }
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    refreshFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';
        let elements = this.extractElements();
        for (let element of Object.keys(elements)) {
            if (elements[element] instanceof Button
                || elements[element] instanceof ButtonGrille
                || elements[element] instanceof ToggleButton
                || elements[element] instanceof ToggleButtonImage
                || elements[element] instanceof ToggleGroup
                || elements[element] instanceof CheckBox)
                elements[element].updateFont(font);
            else if (elements[element] instanceof PIXI.Text)
                elements[element].style.fontFamily = font;

        }
    }

    /**
     * Méthode permettant de récupérer les noms des activités du scénario
     * @param {String} nameAct le string activité
     * @returns {Object} le texte (info), la note (note), la couleur (color)
     */
    getScenario(nameAct) {
        var scenario = JSON.parse(this.refGame.global.resources.getScenario());
        for (var categorie in scenario) {
            for (var subCategorie in scenario[categorie]) {
                for (let i = 0; i < scenario[categorie][subCategorie]['activite'].length; i++) {
                    var acti = scenario[categorie][subCategorie]['activite'][i];
                    for (var actiKey in acti) {
                        if (actiKey == nameAct) {
                            for (var test in acti[actiKey]) {
                                if (categorie == "alimentation") {
                                    var texte = acti[actiKey]['texte'];
                                    var note = acti[actiKey]['note']
                                    var result = new Object();
                                    result.color = COLOR_ALIMENTATION;
                                    result.info = texte;
                                    result.note = note;
                                    console.log("RESULT TEXTE : " + result.info + " RESULT NOTE : " + result.note);
                                    return result;
                                } else if (categorie == "education") {
                                    var texte = acti[actiKey]['texte'];
                                    var note = acti[actiKey]['note']
                                    var result = new Object();
                                    result.color = COLOR_EDUCATION;
                                    result.info = texte;
                                    result.note = note;
                                    console.log("RESULT TEXTE : " + result.info + " RESULT NOTE : " + result.note);
                                    return result;
                                } else if (categorie == "loisirs") {
                                    var texte = acti[actiKey]['texte'];
                                    var note = acti[actiKey]['note']
                                    var result = new Object();
                                    result.color = COLOR_LOISIRS;
                                    result.info = texte;
                                    result.note = note;
                                    console.log("RESULT TEXTE : " + result.info + " RESULT NOTE : " + result.note);
                                    return result;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Pop-up pour ajouter une activité dans la grille.
     */
    popUpEnt() {
        var div;
        this.catEducation = this.refGame.global.resources.getOtherText('catEducation');
        this.catLoisirs = this.refGame.global.resources.getOtherText('catLoisirs');
        this.catAlimentation = this.refGame.global.resources.getOtherText('catAlimentation');
        var color;
        var colorText = 0;
        this.activites = this.refGame.global.resources.getScenario();

        Swal.fire({
            title: name.toUpperCase(),
            width: 900,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: 'Annuler',
            showConfirmButton: true,
            showCancelButton: true,
            html:

                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<div id="jsonContent" style="display: none;">' + this.activites + '</div>' +
                '<div class="center">' +
                '<button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="' + this.catEducation.toLowerCase() + '">' + this.catEducation + '</button>&nbsp;' +
                '<button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="' + this.catLoisirs.toLowerCase() + '">' + this.catLoisirs + '</button>&nbsp;' +
                '<button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="' + this.catAlimentation.toLowerCase() + '">' + this.catAlimentation + '</button>&nbsp;' +
                '</br>' +
                '</br>' +
                '<div class="buttonSousCat"></div>' +
                '<div class="activite"></div>' +
                '</div>' +
                '</tr>' +
                '</tbody>' +
                '</table>' +
                '</div>',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('input[name="radioTrain"]:checked').next()
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value) {
                var texte = result.value[0][0].innerText;
                var end;
                if (texte.length > 38) {
                    var fin = texte.slice(0, 38);
                    end = fin + "...";
                } else {
                    end = texte;
                }
                this.clic.setText(end, color, colorText);
            }
        }.bind(this));

        /////////////////////////////////////
        //    Sous catégorie Education     //
        /////////////////////////////////////
        $(document).on('click', '#' + this.catEducation.toLowerCase(), function () {
            color = 0x9aa7c1;
            var buttons = document.getElementsByClassName("buttonSousCat");
            var activites = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            for (i = 0; i < activites.length; i++) {
                activites[i].innerHTML = "";
            }

            div = '<div style="display:flex; justify-content: center;">' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="ecole">Ecole</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="prive">Prive</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="devoirs">Devoirs</button>&nbsp;</div>' + '</div></br></br>';
            $(div).appendTo(".buttonSousCat");
        });

        /////////////////////////////////////
        //      Activités Education        //
        /////////////////////////////////////
        $(document).on('click', "#ecole", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['education']['ecole']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#prive", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['education']['prive']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#devoirs", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['education']['devoirs']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });

        /////////////////////////////////////
        //     Sous catégorie Loisirs      //
        /////////////////////////////////////
        $(document).on('click', '#' + this.catLoisirs.toLowerCase(), function () {
            color = 0xcd90d8;
            var buttons = document.getElementsByClassName("buttonSousCat");
            var activites = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            for (i = 0; i < activites.length; i++) {
                activites[i].innerHTML = "";
            }
            div = '<div style="display:flex; justify-content: center;">' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="ecran">Ecran</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="lecture">Lecture</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="jeux">Jeux</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="sport">Sport</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="musique">Musique</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="creation">Creation</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="menage">Menage</button>&nbsp;</div>' + '</div></br></br>';
            $(div).appendTo(".buttonSousCat");

        });

        /////////////////////////////////////
        //       Activités Loisirs         //
        /////////////////////////////////////
        $(document).on('click', "#ecran", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['ecran']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#lecture", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['lecture']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#jeux", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['jeux']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#sport", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['sport']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#musique", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['musique']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#creation", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['creation']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#menage", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['menage']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });

        /////////////////////////////////////
        //   Sous catégorie Alimentation   //
        /////////////////////////////////////
        $(document).on('click', '#' + this.catAlimentation.toLowerCase(), function () {
            color = 0xebec9f;
            var buttons = document.getElementsByClassName("buttonSousCat");
            var activites = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            for (i = 0; i < activites.length; i++) {
                activites[i].innerHTML = "";
            }

            div = '<div style="display:flex; justify-content: center;">' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="petitDejeuner">Petit-dejeuner</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="diner">Diner</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="enCas">En-cas</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="souper">Souper</button>&nbsp;</div>' + '</div></br></br>';
            $(div).appendTo(".buttonSousCat");
        });

        /////////////////////////////////////
        //     Activités Alimentation      //
        /////////////////////////////////////
        $(document).on('click', "#petitDejeuner", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['petit-dejeuner']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#diner", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['diner']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#enCas", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['en-cas']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#souper", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['souper']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
    }
}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}
class CreateMode extends Mode{

    constructor(refGame){
        super('create');
        this.refGame = refGame;
        this.setInterfaces({
            create: new Create(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.create.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.create.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.create.refreshFont(isOpendyslexic)
    }
}

class ExploreMode extends Mode{

    constructor(refGame){
        super('explore');
        this.refGame = refGame;
        this.setInterfaces({
            explore: new Explore(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.explore.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.explore.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.explore.refreshFont(isOpendyslexic)
    }
}

class PlayMode extends Mode{

    constructor(refGame){
        super('play');
        this.refGame = refGame;
        this.setInterfaces({
            play: new Play(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.play.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.play.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.play.refreshFont(isOpendyslexic)
    }
}
class TrainMode extends Mode{

    constructor(refGame){
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({
            train: new Train(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(evaluate){
        this.evaluate = evaluate;
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.train.show(this.evaluate);
        var evaluate = this.evaluate;
        this.refGame.showText(this.refGame.global.resources.getTutorialText(evaluate ? 'startEvaluate' : 'startTrain'));
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.train.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.train.refreshFont(isOpendyslexic)
    }
}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.scenes = {
            explore:new PIXI.Container(),
            train:new PIXI.Container(),
            create:new PIXI.Container(),
            play:new PIXI.Container()
        };
        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.playMode = new PlayMode(this);
        this.oldGamemode = undefined;
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        new this.global.Log('success', `Chargement de la scene`).show();
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic) {
        this.createMode.onFontChange(isOpendyslexic);
        this.exploreMode.onFontChange(isOpendyslexic);
        this.trainMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                this.trainMode.init(true);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                this.playMode.init();
                this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
        new this.global.Log('success', 'Mode de jeu change ' + gamemode).show();
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
