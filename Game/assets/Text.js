class Text extends Asset {
    constructor(x, y, text) {
        super();
        this.x = x;
        this.y = y;
        this.text = text;
        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Ajouter notre forme sur l'élément graphique
        let text = new PIXI.Text(this.text,{fontFamily: "\"Arial\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 600, fontSize: 24, fill : 0x000000, align : 'center'});
        text.x = this.x;
        text.y = this.y;


        this.placement.addChild(text);

        this.container.addChild(this.placement);
    }

    getPixiChildren() {
        return [this.container];
    }

    getX(){
        return this.x;
    }

    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }

    setY(y){
        this.y = y;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}