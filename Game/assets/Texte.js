/**
 * @classdesc Asset Texte
 * @author Arnaud Kolly
 * @version 1.0
 */
class Texte extends Asset {
    /**
     * Constructeur de l'asset Texte
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {String} text le texte
     * @param {number} tailleTexte la taille du texte
     * @param {boolean} gras true si on veut en gras sinon false
     */
    constructor(x, y, text, tailleTexte = 12, gras = false) {
        super();
        /** @type {double} la coordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y*/
        this.y = y;
        /** @type {String} le texte */
        this.text = text;
        /** @type {number} la taille du texte */
        this.tailleTexte = tailleTexte;
        /** @type {boolean} texte en gras (true) */
        this.gras = gras;

        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.display();
    }

    /**
     * Méthode permettant d'initialiser et de placer les élements
     */
    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        let text = null;
        if (this.gras) {
            text = new PIXI.Text(this.text, {
                fontFamily: "\"Arial\", cursive, sans-serif",
                fontVariant: "small-caps",
                fontWeight: 600,
                fontSize: this.tailleTexte,
                fill: 0x000000,
            });
        } else {
            text = new PIXI.Text(this.text, {
                fontFamily: "\"Arial\", cursive, sans-serif",
                fontVariant: "small-caps",
                fontSize: this.tailleTexte,
                fill: 0x000000,
            });
        }

        text.x = this.x;
        text.y = this.y;

        this.placement.addChild(text);
        this.container.addChild(this.placement);
    }

    getPixiChildren() {
        return [this.container];
    }

    getX() {
        return this.x;
    }

    setX(x) {
        this.x = x;
    }

    getY() {
        return this.y;
    }

    setY(y) {
        this.y = y;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}