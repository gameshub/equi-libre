class Shape extends Asset {


    constructor(x, y, taille, bgImage, name, onMove, onDBClick) {
        super();
        this.x = x;
        this.y = y;
        this.taille = taille;
        this.bgImage = bgImage;
        this.name = name;
        this.onMove = onMove;
        this.onDBClick = onDBClick;

        this.container = new PIXI.Container();

        this.isFirstClick = true;

        this.data = undefined;
        this.shape = undefined;

        this.init();
    }

    init() {
        this.container.removeChildren();

        this.shape = PIXI.Sprite.fromImage(this.bgImage);
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.taille;
        this.shape.height = this.taille;

        this.shape.interactive = true;
        this.shape.on('pointerdown', this.onClick.bind(this));
        this.shape.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));

        this.container.addChild(this.shape);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500);
        } else {
            this.isFirstClick = true;
            if (this.onDBClick) {
                this.onDBClick(this);
            }
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;

    }

    onDragMove() {
        if (this.data) {
            let newPos = this.data.getLocalPosition(this.shape.parent);

            newPos.x = newPos.x < (0+this.taille/2) ? (0+this.taille/2) : newPos.x;
            newPos.x = newPos.x > (600-this.taille/2) ? (600-this.taille/2) : newPos.x;
            newPos.y = newPos.y < (0+this.taille/2) ? (0+this.taille/2) : newPos.y;
            newPos.y = newPos.y > (600-this.taille/2) ? (600-this.taille/2) : newPos.y;

            this.shape.x = newPos.x;
            this.shape.y = newPos.y;

            if (this.onMove) {
                this.onMove(this);
            }
        }
    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}