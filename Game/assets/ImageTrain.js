/**
 * @classdesc Asset ImageTrain
 * @author Arnaud Kolly
 * @version 1.0
 */
class ImageTrain extends Asset {
    /**
     * Constructeur de l'asset ImageTrain
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {number} taille la taille de l'image
     * @param {String} imgBackground l'image
     */
    constructor(x, y, taille, imgBackground) {
        super();
        /** @type {double} la cordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y */
        this.y = y;
        /** @type {number} la taille de l'image*/
        this.taille = taille;
        /** @type {String} l'image*/
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.onClick = function () {
            console.log('Replace this action with image.setOnClick')
        };

        this.init();
    }

    /**
     * Méthode d'initialisation et de mise en place des élements
     */
    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
