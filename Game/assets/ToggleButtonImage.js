/**
 * @classdesc Asset ToggleButtonImage
 * @author Arnaud Kolly
 * @version 1.0
 */
class ToggleButtonImage extends Asset {

    /**
     * Constructeur de l'asset toggleButtonImage
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {String} label le texte du bouton
     * @param {String} image le nom de l'image
     * @param {boolean} fitToText la taille du bouton en fonction du texte
     * @param {number} width la largeur du bouton
     */
    constructor(x, y, label, image, fitToText = false, width = 150) {
        super();
        /** @type {boolean} la taille du bouton en fonction du texte*/
        this.fitToText = fitToText;
        /** @type la référence au toggle group */
        this.refToggleGroup = null;
        /** @type {number} la largeur du bouton*/
        this.width = width;
        /** @type {number} la hauteur du bouton */
        this.height = 0;
        /** @type les paramètres pour passer au steps suivant par exemple
         * @private*/
        this._data = null;
        /** @type {string} Statut courant du bouton */
        this.currentState = 'inactive';
        /** @type {double} la coordonnée x */
        this.x = x;
        /** @type {double} la coordonnée y */
        this.y = y;
        /** @type {String} le texte du bouton */
        this.text = label;
        /** @type {String} l'image du bouton */
        this.image = image;


        this.lastOnClick = function () {
            this.toggle();
            if (this.refToggleGroup != null) {
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics: new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center'}),
                img: new PIXI.Sprite.fromImage(this.image)

            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics: new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center'}),
                img: new PIXI.Sprite.fromImage(this.image)
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics: new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center'}),
                img: new PIXI.Sprite.fromImage(this.image)
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setObject(this.text, this.image);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setObject(text, image) {
        for (let state of Object.keys(this.states)) {
            this.states[state].lbl.text = text;
            this.states[state].img.imgBackground = image;
        }
        this.update();
    }

    /**
     * Mise à jour de la mise en forme des boutons
     */
    update() {
        for (let state of Object.keys(this.states)) {
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width) : this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if (state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if (state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.x = 0.75;
            this.states[state].lbl.anchor.y = 0.5;
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
            this.states[state].img.x = this.x;
            this.states[state].img.y = this.y;
            this.states[state].img.width = 20;
            this.states[state].img.height = 20;
            this.states[state].img.anchor.x = -2;
            this.states[state].img.anchor.y = 0.5;
        }
    }

    toggle() {
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if (this.refToggleGroup != null) {
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.inactive.img,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.active.img,
            this.states.disabled.graphics,
            this.states.disabled.lbl,
            this.states.disabled.img];
    }

    updateView() {
        for (let state of Object.keys(this.states)) {
            if (state === this.currentState) {
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
                this.states[state].img.visible = true;
            } else {
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
                this.states[state].img.visible = false;
            }
        }
    }

    isActive() {
        return this.currentState === 'active';
    }

    isEnabled() {
        return this.currentState !== 'disabled';
    }

    set data(data) {
        this._data = data;
    }

    get data() {
        return this._data;
    }


    show() {
        this.updateView();
    }

    hide() {
        for (let element of this.getPixiChildren()) {
            element.visible = false;
        }
    }

    disable() {
        this.currentState = 'disabled';
        this.onClick = function () {
        };
        this.updateView();
    }

    enable() {
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref) {
        this.refToggleGroup = ref;
    }

    updateFont(font) {
        for (let state of Object.keys(this.states)) {
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()) {
            element.visible = visible;
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }

    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}