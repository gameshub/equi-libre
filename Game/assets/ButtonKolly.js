class ButtonKolly extends Asset {

    constructor(x, y, text) {
        super();
        this.x = x;
        this.y = y;
        this.text = text;

        this.container = new PIXI.Container();
        this.init();
    }

    init(){
     this.container. removeChildren();
     this.buttonText = new PIXI.Text(this.text,{fontFamily: 'Arial', fontSize: 14, fill: "white", align: 'right'});
     this.buttonText.anchor.set(0.5, 0.5);
     this.buttonText.position.set(40, 25);
     this.buttonEnd = new PIXI.Graphics();
     this.buttonEnd.beginFill(0x0000FF);
     this.buttonEnd.drawRect(0,0,80,50);
     this.buttonEnd.buttonMode = true;
     this.buttonEnd.interactive = true;
     this.container.addChild(this.buttonText);
     this.container.addChild(this.buttonEnd);
    }

    setText(text) {
        this.buttonText.text = text;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
    }

    setX(x) {
        this.x = x;
    }

    getWidth() {
        super.getWidth();
    }

    getHeight() {
        super.getHeight();
    }

    setVisible(visible) {
        super.setVisible(visible);
    }
}