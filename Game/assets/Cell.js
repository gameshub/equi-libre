class Cell extends Asset {


    constructor(x, y, taille, imgBackground) {
        super();
        this.y = y;
        this.x = x;
        this.taille = taille;
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.init();
    }

    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
