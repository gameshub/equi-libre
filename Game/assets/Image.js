class Image extends Asset {


    constructor(x, y, taille, imgBackground, text = '') {
        super();
        this.y = y;
        this.x = x;
        this.taille = taille;
        this.imgBackground = imgBackground;
        this.text = text;

        this.container = new PIXI.Container();

        this.onClick = function () {
            console.log('Replace this action with image.setOnClick')
        };

        this.init();
    }

    init() {
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.taille;
        bg.height = this.taille;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;


        let text = new PIXI.Text(this.text,{fontFamily: "\"Arial\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 600, fontSize: 14, fill : 0x000000, align : 'center'});
        text.x = this.x;
        text.y = this.y;
        text.anchor.x = 0.5;
        text.anchor.y = -3;

        bg.interactive = true;
        bg.buttonMode = true;
        bg.on('pointerdown', function (){
            this.onClick();
        }.bind(this));

        this.container.addChild(bg, text);
    }

    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.taille;
    }

    getHeight() {
        return this.taille;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
