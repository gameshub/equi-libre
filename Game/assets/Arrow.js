class Arrow extends Asset {
    constructor(startX, startY, toX, toY, color, arrow = false, cpXY = null, width = 1, dashed = null) {
        super();
        this.startX = startX;
        this.startY = startY;
        this.toX = toX;
        this.toY = toY;
        this.arrow = arrow;
        this.color = color;
        this.cpXY = cpXY;
        this.dashed = dashed;
        this.width = width;
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        let distX = this.toX - this.startX;
        let distY = this.toY - this.startY;
        let normal = [-(distY), distX]

        let hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
        normal[0] /= hypo;
        normal[1] /= hypo;

        //permet de déplacer le vecteur normal plus vers l'arrière de la ligne
        let tangent = [ -normal[1] * 57, normal[0] * 57]
        normal[0] *= 14;
        normal[1] *= 14;

        let propX = this.startX + (distX / hypo * (hypo-39));
        let propY = this.startY + (distY / hypo * (hypo-39));

        let line = new PIXI.Graphics();

        if(this.cpXY == null){
            let endX = this.toX;
            let endY = this.toY;
            if(this.arrow){
                endX = this.startX + (distX / hypo * (hypo - 47));
                endY = this.startY + (distY / hypo * (hypo - 47));
            }
            line.lineStyle(this.width, this.color, 1);
            line.moveTo(this.startX, this.startY);

            //si c'est traitillé il appelle la méthode pour dessinner les traits
            if(this.dashed == null){
                line.lineTo(endX, endY);
            }else{
                this.drawDashLine(line, this.startX, this.startY, endX, endY, this.dashed[0] ,this.dashed[1]);
            }

        }else{
            line
                .lineStyle(this.width, this.color, 1)
                .moveTo(this.startX, this.startY)
                .bezierCurveTo(this.cpXY[0], this.cpXY[1], this.cpXY[2], this.cpXY[3], this.toX, this.toY);

            //Flèche au bout de la flèche ronde
            if(this.arrow){
                normal = [ -(this.toY - this.cpXY[3]), this.toX - this.cpXY[2]]
                hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
                normal[0] /= hypo;
                normal[1] /= hypo;
                tangent = [ -normal[1] * 20, normal[0] * 20]
                normal[0] *= 14;
                normal[1] *= 14;
                propX = this.toX;
                propY = this.toY;
            }
        }

        //enlever this.cpXY == null si on veut ajouter flèche au bout des courbes
        if(this.arrow && this.cpXY == null){
            line
                //triangle bout flèche
                .beginFill(this.color, 1)
                .lineStyle(0, this.color, 1)
                .moveTo(this.toX - normal[0] + tangent[0], this.toY - normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(propX , propY)
                .endFill();
        }
        this.container.addChild(line);
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getFromX(){
        return this.startX;
    }

    setFromX(fromX){
        this.startX = fromX;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    drawDashLine(line, x, y, toX, toY, tailleTrais, tailleEspace) {
        let distX = toX - x;
        let distY = toY - y;
        let hypo = Math.sqrt(distX ** 2 + distY ** 2);

        let pointHypo = tailleTrais;

        let startX = x;
        let startY = y;

        while (pointHypo <= hypo) {
            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo));

            line.lineTo(startX, startY);

            pointHypo += tailleEspace;

            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo));

            line.moveTo(startX, startY);

            pointHypo += tailleTrais;

            //permet de dessiner un trait plus petit si besoin pour bien correspondre à la traille voulue
            if((pointHypo - hypo) > 0 && (pointHypo - tailleTrais) < hypo){
                pointHypo = hypo;
            }
        }
    };
}