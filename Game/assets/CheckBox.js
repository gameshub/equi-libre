class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}