class Table extends Asset {


    constructor(x, y, width, height, rows, cols) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rows = rows;
        this.cols = cols;

        this.container = new PIXI.Container();

        this.init()
    }

    init() {
        let table = '';
        let rows = this.rows;
        let cols = this.cols;
        for (var r = 0; r < rows; r++) {
            table += '<tr>';
            for (var c = 0; c < cols; c++) {
                table += '<td>' + '' + '</td>';
            }
            table += '</tr>';
        }
        document.createElement("<table border=1>" + table + '</table>');
    }

    getPixiChildren() {
        super.getPixiChildren();
    }

    getY() {
        super.getY();
    }

    getX() {
        super.getX();
    }

    setY(y) {
        this.y = y;
        super.setY(y);
    }

    setX(x) {
        super.setX(x);
    }

    getWidth() {
        super.getWidth();
    }

    getHeight() {
        super.getHeight();
    }

    setVisible(visible) {
        super.setVisible(visible);
    }
}