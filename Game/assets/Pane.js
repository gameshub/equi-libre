class Pane extends Asset{

    constructor(x, y) {
        super();
        this.x = x;
        this.y = y;
        this.container = new PIXI.Container();
        this.init();
    }

    init(){
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

    }

    getPixiChildren() {
        super.getPixiChildren();
    }

    getY() {
        super.getY();
    }

    getX() {
        super.getX();
    }

    setY(y) {
        super.setY(y);
    }

    setX(x) {
        super.setX(x);
    }

    getWidth() {
        super.getWidth();
    }

    getHeight() {
        super.getHeight();
    }

    setVisible(visible) {
        super.setVisible(visible);
    }
}