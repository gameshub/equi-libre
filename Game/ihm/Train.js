/**
 * @classdesc IHM du mode entraîner et évaluer
 * @author Arnaud Kolly
 * @version 1.0
 */

class Train extends Interface {

    /**
     * Constructeur de l'ihm de l'exercice du mode entraîner et évaluer
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "train");
        this.refGame = refGame;
        this.generateGame();
    }

    /**
     * Point d'entrée pour l'affichage
     * @param {boolean} evaluate la variable qui permet de définir si le mode évaluer est sélectionner
     */
    show(evaluate) {
        this.clear();
        this.evaluate = evaluate;
        this.setupElements();

        this.setElements(this.extractElements());

        this.updateStepView();

        this.refGame.showText("");


        this.init();
        this.refreshLang(this.refGame.global.resources.getLanguage());
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        if (this.refGame.global.resources.getExercice() != undefined) {
            this.exercices = JSON.parse(this.refGame.global.resources.getExercice().exercice);
            this.exercicesFinal = this.exercices['planning'];

            for (let i = 0; i < this.exercicesFinal.length; i++) {
                if (this.exercicesFinal[i] != "") {
                    var resultScenario = this.getScenario(this.exercicesFinal[i]);
                    var texte = "" + resultScenario["info"];
                    var color = resultScenario["color"];
                    if (texte != 'undefined') {
                        var end
                        if (texte.length > LONGEUR_TEXTE_GRILLE) {
                            var final = texte.slice(0, LONGEUR_TEXTE_GRILLE);
                            end = final + "...";
                        } else {
                            end = texte;
                        }
                        var result = new Object();
                        result.texteFinal = end;
                        result.colorFinal = color;
                        this.exercicesFinal[i] = result;
                    }
                } else {
                    var result = new Object();
                    result.texteFinal = "";
                    result.colorFinal = COLOR_BUTTON_BLEU;
                    this.exercicesFinal[i] = result;
                }
            }

            this.steps.specific[4].button.setText(this.exercicesFinal[0].texteFinal, this.exercicesFinal[0].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button1.setText(this.exercicesFinal[1].texteFinal, this.exercicesFinal[1].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button2.setText(this.exercicesFinal[2].texteFinal, this.exercicesFinal[2].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button3.setText(this.exercicesFinal[3].texteFinal, this.exercicesFinal[3].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button4.setText(this.exercicesFinal[4].texteFinal, this.exercicesFinal[4].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button5.setText(this.exercicesFinal[5].texteFinal, this.exercicesFinal[5].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button6.setText(this.exercicesFinal[6].texteFinal, this.exercicesFinal[6].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button7.setText(this.exercicesFinal[7].texteFinal, this.exercicesFinal[7].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button8.setText(this.exercicesFinal[8].texteFinal, this.exercicesFinal[8].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button9.setText(this.exercicesFinal[9].texteFinal, this.exercicesFinal[9].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button10.setText(this.exercicesFinal[10].texteFinal, this.exercicesFinal[10].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button11.setText(this.exercicesFinal[11].texteFinal, this.exercicesFinal[11].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button12.setText(this.exercicesFinal[12].texteFinal, this.exercicesFinal[12].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button13.setText(this.exercicesFinal[13].texteFinal, this.exercicesFinal[13].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button14.setText(this.exercicesFinal[14].texteFinal, this.exercicesFinal[14].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button15.setText(this.exercicesFinal[15].texteFinal, this.exercicesFinal[15].colorFinal, COLOR_NOIR);
            this.steps.specific[4].button16.setText(this.exercicesFinal[16].texteFinal, this.exercicesFinal[16].colorFinal, COLOR_NOIR);
        }

        for (let alert of Object.keys(this.alert))
            this.alert[alert] = this.refGame.global.resources.getOtherText(alert);

        for (let i = 0; i < this.steps.specific.length; i++) {
            if (i != 4) {
                this.steps.specific[i]["title".concat(i)].text = this.refGame.global.resources.getOtherText('step' + i + 'Title');
                this.steps.specific[i]["subtitle".concat(i)].text = this.refGame.global.resources.getOtherText('step' + i + 'Subtitle');

                let subtitle = this.steps.specific[i]["subtitle".concat(i)].text;
                this.lang.push(this.steps.specific[i]["title".concat(i)].text + ".<br>" + ((typeof subtitle !== 'undefined') ? subtitle : ''));
                //Update size;
                this.steps.specific[i]["subtitle".concat(i)].y = 85 + this.steps.specific[i]["subtitle".concat(i)].height / 2;

            }
        }


        this.steps.specific[0].btnNext0.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[1].btnBack1.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[1].btnNext1.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[2].btnBack2.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[2].btnNext2.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[3].btnBack3.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[3].btnNext3.setText(this.refGame.global.resources.getOtherText('btnNext'));
        this.steps.specific[4].btnNext4.setText(this.refGame.global.resources.getOtherText('btnSaveAndNext'));
        this.steps.specific[4].btnBack4.setText(this.refGame.global.resources.getOtherText('btnBack'));
        this.steps.specific[5].btnNewExercice.setText(this.refGame.global.resources.getOtherText('btnNewExercice'));
        this.steps.specific[5].btnBack5.setText(this.refGame.global.resources.getOtherText('btnBack'));

        this.steps.general.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: this.refGame.global.resources.getOtherText('mode'),
            harmos: this.refGame.global.resources.getDegre(),
            lang: lang
        });

        this.steps.specific[1].question1.getButtons()[0].setText(this.refGame.global.resources.getOtherText('heure1'));
        this.steps.specific[1].question1.getButtons()[1].setText(this.refGame.global.resources.getOtherText('heure2'));
        this.steps.specific[1].question1.getButtons()[2].setText(this.refGame.global.resources.getOtherText('heure3'));
        this.steps.specific[1].question1.getButtons()[3].setText(this.refGame.global.resources.getOtherText('heure4'));
        this.steps.specific[1].question1.getButtons()[4].setText(this.refGame.global.resources.getOtherText('heure5'));
        this.steps.specific[1].question1.getButtons()[5].setText(this.refGame.global.resources.getOtherText('heure6'));
        this.steps.specific[1].question1.getButtons()[6].setText(this.refGame.global.resources.getOtherText('heure7'));
        this.steps.specific[1].question1.getButtons()[7].setText(this.refGame.global.resources.getOtherText('heure8'));
        this.steps.specific[1].question1.getButtons()[8].setText(this.refGame.global.resources.getOtherText('heure9'));
        this.steps.specific[1].question1.getButtons()[9].setText(this.refGame.global.resources.getOtherText('heure10'));


        this.steps.specific[2].question2.getButtons()[0].setText(this.refGame.global.resources.getOtherText('dent1'));
        this.steps.specific[2].question2.getButtons()[1].setText(this.refGame.global.resources.getOtherText('dent2'));
        this.steps.specific[2].question2.getButtons()[2].setText(this.refGame.global.resources.getOtherText('dent3'));
        this.steps.specific[2].question2.getButtons()[3].setText(this.refGame.global.resources.getOtherText('dent4'));
        this.steps.specific[2].question2.getButtons()[4].setText(this.refGame.global.resources.getOtherText('dent5'));
        this.steps.specific[2].question2.getButtons()[5].setText(this.refGame.global.resources.getOtherText('dent6'));

        this.steps.specific[3].texte.text = this.refGame.global.resources.getOtherText('depTexte2');

        this.steps.specific[3].question3.getButtons()[0].setObject(this.refGame.global.resources.getOtherText('train'));
        this.steps.specific[3].question3.getButtons()[1].setObject(this.refGame.global.resources.getOtherText('bus'));
        this.steps.specific[3].question3.getButtons()[2].setObject(this.refGame.global.resources.getOtherText('voiture'));
        this.steps.specific[3].question3.getButtons()[3].setObject(this.refGame.global.resources.getOtherText('velo'));
        this.steps.specific[3].question3.getButtons()[4].setObject(this.refGame.global.resources.getOtherText('trottinette'));
        this.steps.specific[3].question3.getButtons()[5].setObject(this.refGame.global.resources.getOtherText('pied'));

        this.steps.specific[3].questionTrain.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionTrain.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionTrain.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionTrain.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionBus.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionBus.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionBus.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionBus.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionVoiture.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionVoiture.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionVoiture.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionVoiture.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionVelo.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionVelo.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionVelo.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionVelo.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionTrottinette.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionTrottinette.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionTrottinette.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionTrottinette.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[3].questionPied.getButtons()[0].setText(this.refGame.global.resources.getOtherText('number1'));
        this.steps.specific[3].questionPied.getButtons()[1].setText(this.refGame.global.resources.getOtherText('number2'));
        this.steps.specific[3].questionPied.getButtons()[2].setText(this.refGame.global.resources.getOtherText('number3'));
        this.steps.specific[3].questionPied.getButtons()[3].setText(this.refGame.global.resources.getOtherText('number4'));

        this.steps.specific[4].label0.text = this.refGame.global.resources.getOtherText("tabLever");
        this.steps.specific[4].label1.text = this.refGame.global.resources.getOtherText("tabMatinee");
        this.steps.specific[4].label2.text = this.refGame.global.resources.getOtherText("tabMidi");
        this.steps.specific[4].label3.text = this.refGame.global.resources.getOtherText("tabApm");
        this.steps.specific[4].label4.text = this.refGame.global.resources.getOtherText("tabSoir");

        this.steps.specific[5].txtAlim.setVisible(false);

    }

    generateGame() {

        this.currentStep = 0;

        this.labelStyle = new PIXI.TextStyle();
        this.titleStyle = new PIXI.TextStyle();
        this.subtitleStyle = new PIXI.TextStyle();


        this.titleStyle.align = ALIGNEMENT_CENTER;
        this.titleStyle.fill = COLOR_NOIR;
        this.titleStyle.fontFamily = FONTFAMILY_ARIAL;
        this.titleStyle.fontSize = 32;
        this.subtitleStyle.align = ALIGNEMENT_CENTER;
        this.subtitleStyle.fill = COLOR_NOIR;
        this.subtitleStyle.fontFamily = FONTFAMILY_ARIAL;
        this.subtitleStyle.fontSize = 18;
        this.subtitleStyle.wordWrap = true;
        this.subtitleStyle.wordWrapWidth = 550;
        this.labelStyle.fontFamily = FONTFAMILY_ARIAL;
        this.labelStyle.fontSize = 22;
        this.labelStyle.align = ALIGNEMENT_LEFT;
        this.labelStyle.fill = COLOR_NOIR;

        /**
         * Variable contenant tous les messages d'alerte.
         * @type {{noType: string, confirmDeletionText: string, noNameText: string, checkConditionText: string, checkCondition: string, noName: string, confirmDeletion: string, noTypeText: string}}
         */
        this.alert = {
            noHeure: '',
            noHeureText: '',
            noDent: '',
            noDentText: '',
            noDeplacement: '',
            noDeplacementText: '',
            noNbDeplacement: '',
            noNbDeplacementText: ''
        };

        /**
         *
         * @type {{general: {}, specific: *[]}}} Variable contenant tous les éléments graphiques pour les différentes interfaces.
         */
        this.steps = {
            specific: [
                // 0 Accueil
                {
                    btnNext0: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    title0: new PIXI.Text('', this.titleStyle),
                    subtitle0: new PIXI.Text('', this.subtitleStyle),
                },
                // 1 Heures de sommeil
                {

                    btnBack1: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    btnNext1: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, true),
                    image1: new ImageTrain(300, 450, 150, this.refGame.global.resources.getImage('sommeil').image.src),
                    title1: new PIXI.Text('', this.titleStyle),
                    subtitle1: new PIXI.Text('', this.subtitleStyle),
                    question1: new ToggleGroup('single', [
                        new ToggleButton(200, 160, '', false),
                        new ToggleButton(200, 200, '', false),
                        new ToggleButton(200, 240, '', false),
                        new ToggleButton(200, 280, '', false),
                        new ToggleButton(200, 320, '', false),
                        new ToggleButton(400, 160, '', false),
                        new ToggleButton(400, 200, '', false),
                        new ToggleButton(400, 240, '', false),
                        new ToggleButton(400, 280, '', false),
                        new ToggleButton(400, 320, '', false)
                    ])
                },
                // 2 = Brossage de dent
                {
                    btnBack2: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    btnNext2: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    image2: new ImageTrain(300, 450, 150, this.refGame.global.resources.getImage('dent').image.src),
                    title2: new PIXI.Text('', this.titleStyle),
                    subtitle2: new PIXI.Text('', this.subtitleStyle),
                    question2: new ToggleGroup('single', [
                        new ToggleButton(200, 160, '', false),
                        new ToggleButton(200, 200, '', false),
                        new ToggleButton(200, 240, '', false),
                        new ToggleButton(400, 160, '', false),
                        new ToggleButton(400, 200, '', false),
                        new ToggleButton(400, 240, '', false)
                    ])
                },
                // 3 = Déplacement
                {
                    btnBack3: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    btnNext3: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC),
                    title3: new PIXI.Text('', this.titleStyle),
                    subtitle3: new PIXI.Text('', this.subtitleStyle),
                    texte: new PIXI.Text('', this.subtitleStyle),
                    question3: new ToggleGroup('single', [
                        new ToggleButtonImage(200, 150, '', this.refGame.global.resources.getImage('train').image.src, false),
                        new ToggleButtonImage(200, 190, '', this.refGame.global.resources.getImage('bus').image.src, false),
                        new ToggleButtonImage(200, 230, '', this.refGame.global.resources.getImage('voiture').image.src, false),
                        new ToggleButtonImage(400, 150, '', this.refGame.global.resources.getImage('velo').image.src, false),
                        new ToggleButtonImage(400, 190, '', this.refGame.global.resources.getImage('trottinette').image.src, false),
                        new ToggleButtonImage(400, 230, '', this.refGame.global.resources.getImage('pied').image.src, false)
                    ]),
                    imageTrain: new ImageTrain(250, 340, 50, this.refGame.global.resources.getImage('train').image.src),
                    imageBus: new ImageTrain(250, 420, 50, this.refGame.global.resources.getImage('bus').image.src),
                    imageVoiture: new ImageTrain(250, 500, 50, this.refGame.global.resources.getImage('voiture').image.src),
                    imageVelo: new ImageTrain(500, 340, 50, this.refGame.global.resources.getImage('velo').image.src),
                    imageTrottinette: new ImageTrain(500, 420, 50, this.refGame.global.resources.getImage('trottinette').image.src),
                    imagePied: new ImageTrain(500, 500, 50, this.refGame.global.resources.getImage('pied').image.src),
                    questionTrain: new ToggleGroup('single', [
                        new ToggleButton(80, 340, '', true),
                        new ToggleButton(120, 340, '', true),
                        new ToggleButton(160, 340, '', true),
                        new ToggleButton(200, 340, '', true)
                    ]),
                    questionBus: new ToggleGroup('single', [
                        new ToggleButton(80, 420, '', true),
                        new ToggleButton(120, 420, '', true),
                        new ToggleButton(160, 420, '', true),
                        new ToggleButton(200, 420, '', true)
                    ]),
                    questionVoiture: new ToggleGroup('single', [
                        new ToggleButton(80, 500, '', true),
                        new ToggleButton(120, 500, '', true),
                        new ToggleButton(160, 500, '', true),
                        new ToggleButton(200, 500, '', true)
                    ]),
                    questionVelo: new ToggleGroup('single', [
                        new ToggleButton(335, 340, '', true),
                        new ToggleButton(375, 340, '', true),
                        new ToggleButton(415, 340, '', true),
                        new ToggleButton(455, 340, '', true)
                    ]),
                    questionTrottinette: new ToggleGroup('single', [
                        new ToggleButton(335, 420, '', true),
                        new ToggleButton(375, 420, '', true),
                        new ToggleButton(415, 420, '', true),
                        new ToggleButton(455, 420, '', true)
                    ]),
                    questionPied: new ToggleGroup('single', [
                        new ToggleButton(335, 500, '', true),
                        new ToggleButton(375, 500, '', true),
                        new ToggleButton(415, 500, '', true),
                        new ToggleButton(455, 500, '', true)
                    ]),
                },
                // 4 = Grille exercice
                {
                    title4: new PIXI.Text('', this.titleStyle),
                    subtitle4: new PIXI.Text('', this.subtitleStyle),
                    btnBack4: new Button(525, 520, '', COLOR_BUTTON_VERT, COLOR_BLANC, false, 100),
                    btnNext4: new Button(525, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, true, 100),
                    //Objets pour les colonnes et contour
                    arrow: new Arrow(1, 0, 1, 600, COLOR_NOIR),
                    arrow1: new Arrow(150, 0, 150, 600, COLOR_NOIR),
                    arrow2: new Arrow(449, 0, 449, 600, COLOR_NOIR),
                    arrow3: new Arrow(1, 1, 449, 1, COLOR_NOIR),
                    arrow4: new Arrow(1, 599, 449, 599, COLOR_NOIR),
                    //Objets pour les lignes de separation
                    arrow5: new Arrow(1, 105.9, 449, 105.9, COLOR_NOIR),
                    arrow6: new Arrow(1, 247.1, 449, 247.1, COLOR_NOIR),
                    arrow7: new Arrow(1, 353, 449, 353, COLOR_NOIR),
                    arrow8: new Arrow(1, 494.2, 449, 494.2, COLOR_NOIR),

                    //Objets pour les lignes par categories
                    arrow9: new Arrow(150, 35.3, 449, 35.3, COLOR_NOIR),
                    arrow10: new Arrow(150, 70.6, 449, 70.6, COLOR_NOIR),
                    arrow11: new Arrow(150, 141.2, 449, 141.2, COLOR_NOIR),
                    arrow12: new Arrow(150, 176.5, 449, 176.5, COLOR_NOIR),
                    arrow13: new Arrow(150, 211.8, 449, 211.8, COLOR_NOIR),
                    arrow14: new Arrow(150, 282.4, 449, 282.4, COLOR_NOIR),
                    arrow15: new Arrow(150, 317.7, 449, 317.7, COLOR_NOIR),

                    arrow16: new Arrow(150, 388.3, 449, 388.3, COLOR_NOIR),
                    arrow17: new Arrow(150, 423.6, 449, 423.6, COLOR_NOIR),
                    arrow18: new Arrow(150, 458.9, 449, 458.9, COLOR_NOIR),
                    arrow19: new Arrow(150, 529.5, 449, 529.5, COLOR_NOIR),
                    arrow20: new Arrow(150, 564.8, 449, 564.8, COLOR_NOIR),

                    label0: new PIXI.Text('', this.labelStyle),
                    label1: new PIXI.Text('', this.labelStyle),
                    label2: new PIXI.Text('', this.labelStyle),
                    label3: new PIXI.Text('', this.labelStyle),
                    label4: new PIXI.Text('', this.labelStyle),

                    //Buttons dans la grille
                    button: new ButtonGrille(300, 18, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button1: new ButtonGrille(300, 53, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button2: new ButtonGrille(300, 88, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button3: new ButtonGrille(300, 123, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button4: new ButtonGrille(300, 159, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button5: new ButtonGrille(300, 194, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button6: new ButtonGrille(300, 229, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button7: new ButtonGrille(300, 265, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button8: new ButtonGrille(300, 300, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button9: new ButtonGrille(300, 335, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button10: new ButtonGrille(300, 370, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button11: new ButtonGrille(300, 406, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button12: new ButtonGrille(300, 442, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button13: new ButtonGrille(300, 476, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button14: new ButtonGrille(300, 512, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button15: new ButtonGrille(300, 546, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),
                    button16: new ButtonGrille(300, 582, '', COLOR_BUTTON_BLEU, COLOR_BLANC, false, 25, 295),

                },
                // 5 = Résultat
                {
                    title5: new PIXI.Text('', this.titleStyle),
                    subtitle5: new PIXI.Text('', this.subtitleStyle),
                    btnBack5: new Button(100, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, false, 100),
                    btnNewExercice: new Button(500, 570, '', COLOR_BUTTON_VERT, COLOR_BLANC, true, 100),
                    arrowRes: new Arrow(100, 100, 100, 400),
                    arrowRes1: new Arrow(100, 100, 90, 125),
                    arrowRes2: new Arrow(100, 100, 110, 125),
                    arrowRes3: new Arrow(100, 400, 500, 400),
                    imageOk: new ImageResult(50, 130, 70, this.refGame.global.resources.getImage('camera').image.src),
                    imageMi: new ImageResult(50, 250, 70, this.refGame.global.resources.getImage('camera').image.src),
                    imageKo: new ImageResult(50, 370, 70, this.refGame.global.resources.getImage('camera').image.src),
                    btnResultAlim: new ButtonResult(150, 250, 255, 0Xffffff, false, 300, 60),
                    resultAlimTexte: new Texte(112, 405, 'Alimentation'),
                    txtAlim: new Texte(0, 450, 'Bravo ! Tu as atteint le nombre maximal de points pour la categorie Alimentation.', 14)

                }
            ],
            general: {
                info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: COLOR_NOIR, align: 'left'})
            }
        }
        ;

        this.lang = [];
    }

    /**
     * Paramétrage de la mise en page et des actions sur les différents boutons
     */
    setupElements() {
        /////////////////////////////////////
        //              General            //
        /////////////////////////////////////

        this.steps.general.info.x = 10;
        this.steps.general.info.y = 10;
        this.steps.general.info.anchor.set(0.5);
        this.steps.general.info.visible = false;

        /**
         *
         * @type {(ButtonGrille|ButtonGrille)[]} Tableau des boutons de la grille.
         */
        this.buttons = [
            this.steps.specific[4].button,
            this.steps.specific[4].button1,
            this.steps.specific[4].button2,
            this.steps.specific[4].button3,
            this.steps.specific[4].button4,
            this.steps.specific[4].button5,
            this.steps.specific[4].button6,
            this.steps.specific[4].button7,
            this.steps.specific[4].button8,
            this.steps.specific[4].button9,
            this.steps.specific[4].button10,
            this.steps.specific[4].button11,
            this.steps.specific[4].button12,
            this.steps.specific[4].button13,
            this.steps.specific[4].button14,
            this.steps.specific[4].button15,
            this.steps.specific[4].button16
        ];

        this.steps.specific[0].btnNext0.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnBack1.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnNext1.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[2].btnBack2.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[2].btnNext2.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnBack3.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnNext3.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnBack4.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnNext4.setOnClick(function () {
            console.log("VALIDER")
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].button.setOnClick(function () {
            this.clic = this.buttons[0];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button1.setOnClick(function () {
            this.clic = this.buttons[1];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button2.setOnClick(function () {
            this.clic = this.buttons[2];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button3.setOnClick(function () {
            this.clic = this.buttons[3];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button4.setOnClick(function () {
            this.clic = this.buttons[4];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button5.setOnClick(function () {
            this.clic = this.buttons[5];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button6.setOnClick(function () {
            this.clic = this.buttons[6];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button7.setOnClick(function () {
            this.clic = this.buttons[7];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button8.setOnClick(function () {
            this.clic = this.buttons[8];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button9.setOnClick(function () {
            this.clic = this.buttons[9];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button10.setOnClick(function () {
            this.clic = this.buttons[10];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button11.setOnClick(function () {
            this.clic = this.buttons[11];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button12.setOnClick(function () {
            this.clic = this.buttons[12];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button13.setOnClick(function () {
            this.clic = this.buttons[13];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button14.setOnClick(function () {
            this.clic = this.buttons[14];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button15.setOnClick(function () {
            this.clic = this.buttons[15];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[4].button16.setOnClick(function () {
            this.clic = this.buttons[16];
            this.popUpEnt();
        }.bind(this));
        this.steps.specific[5].btnBack5.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[5].btnNewExercice.setOnClick(function () {
            console.log("btnNewExercice");
        }.bind(this));

        for (let i = 0; i < this.steps.specific.length; i++) {
            this.steps.specific[i]["title".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["subtitle".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["title".concat(i)].x = 300;
            this.steps.specific[i]["title".concat(i)].y = 50;
            this.steps.specific[i]["subtitle".concat(i)].x = 300;
            this.steps.specific[i]["subtitle".concat(i)].y = 85;
        }


        /////////////////////////////////////
        //              Step 0             //
        /////////////////////////////////////

        for (let potentialForm of Object.keys(this.steps.specific[0])) {
            if (potentialForm.startsWith("form"))
                this.steps.specific[0][potentialForm].visible = false;
        }

        /////////////////////////////////////
        //              Step 1             //
        /////////////////////////////////////

        this.steps.specific[1].question1.getButtons()[0].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[1].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[2].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[3].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[4].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[5].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[6].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[7].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[8].data = {
            nextStep: 2
        };
        this.steps.specific[1].question1.getButtons()[9].data = {
            nextStep: 2
        };

        /////////////////////////////////////
        //              Step 2             //
        /////////////////////////////////////

        this.steps.specific[2].question2.getButtons()[0].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[1].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[2].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[3].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[4].data = {
            nextStep: 3
        };
        this.steps.specific[2].question2.getButtons()[5].data = {
            nextStep: 3
        };

        /////////////////////////////////////
        //              Step 3             //
        /////////////////////////////////////

        this.steps.specific[3].question3.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[4].data = {
            nextStep: 4
        };
        this.steps.specific[3].question3.getButtons()[5].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrain.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionBus.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVoiture.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionVelo.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionTrottinette.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[0].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[1].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[2].data = {
            nextStep: 4
        };
        this.steps.specific[3].questionPied.getButtons()[3].data = {
            nextStep: 4
        };
        this.steps.specific[3].texte.anchor.set(0.5);
        this.steps.specific[3].texte.x = 300;
        this.steps.specific[3].texte.y = 285;

        /////////////////////////////////////
        //              Step 4             //
        /////////////////////////////////////

        this.steps.specific[4].label0.x = 50;
        this.steps.specific[4].label0.y = 35;
        this.steps.specific[4].label1.x = 40;
        this.steps.specific[4].label1.y = 160;
        this.steps.specific[4].label2.x = 60;
        this.steps.specific[4].label2.y = 285;
        this.steps.specific[4].label3.x = 25;
        this.steps.specific[4].label3.y = 410;
        this.steps.specific[4].label4.x = 60;
        this.steps.specific[4].label4.y = 535;

        /////////////////////////////////////
        //              Step 5             //
        /////////////////////////////////////


        this.steps.specific[5].btnResultAlim.setOnClick(function () {
            this.steps.specific[5].txtAlim.setVisible(true);
        }.bind(this));

    }

    /**
     * Méthode retournant tous les éléments graphiques
     * @param {boolean} onlySteps Choix de prendre uniquement les éléments correspondant à une étape spécifique
     * @return {{}} JSON des éléments
     */
    extractElements(onlySteps = false) {
        let elements = {};
        if (!onlySteps)
            for (let element of Object.keys(this.steps.general))
                elements[element] = this.steps.general[element];
        for (let step of this.steps.specific)
            for (let element of Object.keys(step))
                elements[element] = step[element];
        return elements;
    }

    /**
     * Mise à jour de l'interface en fonction de l'étape.
     */
    updateStepView() {
        this.hideAll();
        for (let elementToDisplay of Object.keys(this.steps.specific[this.currentStep])) {
            if (this.currentStep === 0 &&
                (elementToDisplay.startsWith("form") || elementToDisplay.startsWith("btnHideShapes")))
                continue;
            if (this.steps.specific[this.currentStep][elementToDisplay] instanceof Button)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ImageTrain)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ImageResult)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleButton
                || this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleButtonImage
                || this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleGroup)
                this.steps.specific[this.currentStep][elementToDisplay].show();
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof CheckBox)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof Arrow)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ButtonGrille)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ButtonResult)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            // Affichage des noms de catégorie mais pas des texte de résultat
            else if (this.currentStep == 5) {
                this.steps.specific[this.currentStep].resultAlimTexte.setVisible(true);
                this.steps.specific[this.currentStep].txtAlim.setVisible(false);
            } else {
                this.steps.specific[this.currentStep][elementToDisplay].visible = true;
            }
        }
        if (this.currentStep == 4) {
            this.refGame.showText(this.refGame.global.resources.getOtherText('step4Title'));
        } else {
            this.refGame.showText(this.lang[this.currentStep]);
        }
    }

    /**
     * Cache tous les éléments présents sur l'interface
     */
    hideAll() {
        let elements = this.extractElements(true);
        for (let elementName of Object.keys(elements))
            if (elements[elementName] instanceof Button)
                this.setButtonVisible(elements[elementName], false);
            else if (elements[elementName] instanceof ImageTrain)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof ImageResult)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof ToggleButton || elements[elementName] instanceof ToggleButtonImage || elements[elementName] instanceof ToggleGroup)
                elements[elementName].hide();
            else if (elements[elementName] instanceof CheckBox)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof Arrow)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof Texte)
                elements[elementName].setVisible(false);
            else if (elements[elementName] instanceof ButtonResult)
                this.setButtonVisible(elements[elementName], false);
            else if (elements[elementName] instanceof ButtonGrille)
                this.setButtonVisible(elements[elementName], false);
            else
                elements[elementName].visible = false;
    }

    /**
     * Affichage / Désaffichage d'un bouton.
     * @param {Button} button Bouton à modifier
     * @param {boolean} visible Le bouton doit-il être visible
     */
    setButtonVisible(button, visible) {
        for (let pixiElement of button.getPixiChildren()) {
            pixiElement.visible = visible;
        }
    }

    /**
     * Choix de l'étape suivante en fonction de l'étape actuelle;
     */
    chooseNextStep() {
        switch (this.currentStep) {
            case 1:
                if (this.steps.specific[1].question1.getActives().length > 0) {
                    this.currentStep = this.steps.specific[1].question1.getActives()[0].data.nextStep;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noHeureText, this.alert.noHeure);
                break;
            case 2:
                if (this.steps.specific[2].question2.getActives().length > 0) {
                    this.currentStep = this.steps.specific[2].question2.getActives()[0].data.nextStep;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noDentText, this.alert.noDent);
                break;
            case 3:
                if (this.steps.specific[3].question3.getActives().length > 0) {
                    if (this.steps.specific[3].questionTrain.getActives().length > 0 ||
                        this.steps.specific[3].questionBus.getActives().length > 0 ||
                        this.steps.specific[3].questionVoiture.getActives().length > 0 ||
                        this.steps.specific[3].questionVelo.getActives().length > 0 ||
                        this.steps.specific[3].questionTrottinette.getActives().length > 0 ||
                        this.steps.specific[3].questionPied.getActives().length > 0) {
                        this.currentStep++;
                    } else {
                        Util.getInstance().showAlert('warning', this.alert.noNbDeplacementText, this.alert.noNbDeplacement);
                    }
                } else {
                    Util.getInstance().showAlert('warning', this.alert.noDeplacementText, this.alert.noDeplacement);
                }
                break;
            case 4:
                this.currentStep++;
                break;
            case 5:
                this.currentStep++;
                break;
            default :
                this.currentStep = this.currentStep < 10 ? this.currentStep + 1 : 10;
                break;
        }
    }

    /**
     * Choix de l'étape précédante en fonction de l'étape actuelle.
     */
    chooseBackStep() {
        switch (this.currentStep) {
            case 1:
                this.currentStep = 0;
                break;
            case 2:
                this.currentStep = 1;
                break;
            case 3:
                this.currentStep = 2;
                break;
            case 4:
                this.currentStep = 3;
                break;
            case 5:
                this.currentStep = 4;
                break;
            default :
                this.currentStep = this.currentStep > 0 ? this.currentStep - 1 : 0;
                break;
        }
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    refreshFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';
        let elements = this.extractElements();
        for (let element of Object.keys(elements)) {
            if (elements[element] instanceof Button
                || elements[element] instanceof ButtonGrille
                || elements[element] instanceof ToggleButton
                || elements[element] instanceof ToggleButtonImage
                || elements[element] instanceof ToggleGroup
                || elements[element] instanceof CheckBox)
                elements[element].updateFont(font);
            else if (elements[element] instanceof PIXI.Text)
                elements[element].style.fontFamily = font;

        }
    }

    /**
     * Méthode permettant de récupérer les noms des activités du scénario
     * @param {String} nameAct le string activité
     * @returns {Object} le texte (info), la note (note), la couleur (color)
     */
    getScenario(nameAct) {
        var scenario = JSON.parse(this.refGame.global.resources.getScenario());
        for (var categorie in scenario) {
            for (var subCategorie in scenario[categorie]) {
                for (let i = 0; i < scenario[categorie][subCategorie]['activite'].length; i++) {
                    var acti = scenario[categorie][subCategorie]['activite'][i];
                    for (var actiKey in acti) {
                        if (actiKey == nameAct) {
                            for (var test in acti[actiKey]) {
                                if (categorie == "alimentation") {
                                    var texte = acti[actiKey]['texte'];
                                    var note = acti[actiKey]['note']
                                    var result = new Object();
                                    result.color = COLOR_ALIMENTATION;
                                    result.info = texte;
                                    result.note = note;
                                    console.log("RESULT TEXTE : " + result.info + " RESULT NOTE : " + result.note);
                                    return result;
                                } else if (categorie == "education") {
                                    var texte = acti[actiKey]['texte'];
                                    var note = acti[actiKey]['note']
                                    var result = new Object();
                                    result.color = COLOR_EDUCATION;
                                    result.info = texte;
                                    result.note = note;
                                    console.log("RESULT TEXTE : " + result.info + " RESULT NOTE : " + result.note);
                                    return result;
                                } else if (categorie == "loisirs") {
                                    var texte = acti[actiKey]['texte'];
                                    var note = acti[actiKey]['note']
                                    var result = new Object();
                                    result.color = COLOR_LOISIRS;
                                    result.info = texte;
                                    result.note = note;
                                    console.log("RESULT TEXTE : " + result.info + " RESULT NOTE : " + result.note);
                                    return result;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Pop-up pour ajouter une activité dans la grille.
     */
    popUpEnt() {
        var div;
        this.catEducation = this.refGame.global.resources.getOtherText('catEducation');
        this.catLoisirs = this.refGame.global.resources.getOtherText('catLoisirs');
        this.catAlimentation = this.refGame.global.resources.getOtherText('catAlimentation');
        var color;
        var colorText = 0;
        this.activites = this.refGame.global.resources.getScenario();

        Swal.fire({
            title: name.toUpperCase(),
            width: 900,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: 'Annuler',
            showConfirmButton: true,
            showCancelButton: true,
            html:

                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<div id="jsonContent" style="display: none;">' + this.activites + '</div>' +
                '<div class="center">' +
                '<button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="' + this.catEducation.toLowerCase() + '">' + this.catEducation + '</button>&nbsp;' +
                '<button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="' + this.catLoisirs.toLowerCase() + '">' + this.catLoisirs + '</button>&nbsp;' +
                '<button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="' + this.catAlimentation.toLowerCase() + '">' + this.catAlimentation + '</button>&nbsp;' +
                '</br>' +
                '</br>' +
                '<div class="buttonSousCat"></div>' +
                '<div class="activite"></div>' +
                '</div>' +
                '</tr>' +
                '</tbody>' +
                '</table>' +
                '</div>',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('input[name="radioTrain"]:checked').next()
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value) {
                var texte = result.value[0][0].innerText;
                var end;
                if (texte.length > 38) {
                    var fin = texte.slice(0, 38);
                    end = fin + "...";
                } else {
                    end = texte;
                }
                this.clic.setText(end, color, colorText);
            }
        }.bind(this));

        /////////////////////////////////////
        //    Sous catégorie Education     //
        /////////////////////////////////////
        $(document).on('click', '#' + this.catEducation.toLowerCase(), function () {
            color = 0x9aa7c1;
            var buttons = document.getElementsByClassName("buttonSousCat");
            var activites = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            for (i = 0; i < activites.length; i++) {
                activites[i].innerHTML = "";
            }

            div = '<div style="display:flex; justify-content: center;">' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="ecole">Ecole</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="prive">Prive</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #9aa7c1; border-color: #9aa7c1; color: black;" type="button" id="devoirs">Devoirs</button>&nbsp;</div>' + '</div></br></br>';
            $(div).appendTo(".buttonSousCat");
        });

        /////////////////////////////////////
        //      Activités Education        //
        /////////////////////////////////////
        $(document).on('click', "#ecole", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['education']['ecole']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#prive", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['education']['prive']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#devoirs", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['education']['devoirs']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });

        /////////////////////////////////////
        //     Sous catégorie Loisirs      //
        /////////////////////////////////////
        $(document).on('click', '#' + this.catLoisirs.toLowerCase(), function () {
            color = 0xcd90d8;
            var buttons = document.getElementsByClassName("buttonSousCat");
            var activites = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            for (i = 0; i < activites.length; i++) {
                activites[i].innerHTML = "";
            }
            div = '<div style="display:flex; justify-content: center;">' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="ecran">Ecran</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="lecture">Lecture</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="jeux">Jeux</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="sport">Sport</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="musique">Musique</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="creation">Creation</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #cd90d8; border-color: #cd90d8; color: black;" type="button" id="menage">Menage</button>&nbsp;</div>' + '</div></br></br>';
            $(div).appendTo(".buttonSousCat");

        });

        /////////////////////////////////////
        //       Activités Loisirs         //
        /////////////////////////////////////
        $(document).on('click', "#ecran", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['ecran']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#lecture", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['lecture']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#jeux", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['jeux']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#sport", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['sport']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#musique", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['musique']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#creation", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['creation']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#menage", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['loisirs']['menage']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });

        /////////////////////////////////////
        //   Sous catégorie Alimentation   //
        /////////////////////////////////////
        $(document).on('click', '#' + this.catAlimentation.toLowerCase(), function () {
            color = 0xebec9f;
            var buttons = document.getElementsByClassName("buttonSousCat");
            var activites = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            for (i = 0; i < activites.length; i++) {
                activites[i].innerHTML = "";
            }

            div = '<div style="display:flex; justify-content: center;">' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="petitDejeuner">Petit-dejeuner</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="diner">Diner</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="enCas">En-cas</button>&nbsp;</div>' +
                '<div style="padding: 1px"><button class="btn btn-lg btn-primary" style="background-color: #ebec9f; border-color: #ebec9f; color: black;" type="button" id="souper">Souper</button>&nbsp;</div>' + '</div></br></br>';
            $(div).appendTo(".buttonSousCat");
        });

        /////////////////////////////////////
        //     Activités Alimentation      //
        /////////////////////////////////////
        $(document).on('click', "#petitDejeuner", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['petit-dejeuner']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#diner", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['diner']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#enCas", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['en-cas']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
        $(document).on('click', "#souper", function () {
            var buttons = document.getElementsByClassName("activite");
            var i;
            for (i = 0; i < buttons.length; i++) {
                buttons[i].innerHTML = "";
            }
            var div;
            div = '<form style="text-align: left;">';
            var jsonContent = document.getElementById("jsonContent").textContent;
            var jsonParse = JSON.parse(jsonContent);
            var resultJsonParse = jsonParse['alimentation']['souper']['activite'];
            for (var activites in resultJsonParse) {
                Object.keys(resultJsonParse[activites]).forEach(function (key) {
                    if (key != "note") {
                        div = div + '<input type="radio" id="' + key + '" name="radioTrain" value="' + key + '">&nbsp;' +
                            '<label for="' + key + '">' + resultJsonParse[activites][key]['texte'] + '</label><br>';
                    }
                });
            }
            div = div + '</form>';
            $(div).appendTo(".activite");
        });
    }
}