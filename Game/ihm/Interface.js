/**
 * @classdesc Définition d'une interface (ihm)
 * @author Arnaud Kolly
 * @version 1.1
 */

/**
 * Couleur pour la catégorie Loisirs
 * @type {number} Couleur
 */
const COLOR_LOISIRS = 13471960;
/**
 * Couleur pour la catégorie Education
 * @type {number} Couleur
 */
const COLOR_EDUCATION = 10135489;
/**
 * Couleur pour la catégorie Alimentation
 * @type {number} Couleur
 */
const COLOR_ALIMENTATION = 15461535;
/**
 * Couleur bleu pour les boutons
 * @type {number} Couleur bleu
 */
const COLOR_BUTTON_BLEU = 32767;
/**
 * Couleur vert pour les boutons
 * @type {number} Couleur vert
 */
const COLOR_BUTTON_VERT = 43520;
/**
 * Couleur noir pour le texte
 * @type {number} Couleur noir
 */
const COLOR_NOIR = 0;
/**
 * Couleur blanc pour le texte
 * @type {number} Couleur blanc
 */
const COLOR_BLANC = 16777215;
/**
 * Longeur du texte dans la grille
 * @type {number} Longeur
 */
const LONGEUR_TEXTE_GRILLE = 38;
/**
 * Font Family Arial
 * @type {string} font style arial
 */
const FONTFAMILY_ARIAL = 'Arial';
/**
 * Alignement à centrer
 * @type {string} Alignement centrer
 */
const ALIGNEMENT_CENTER = 'center';
/**
 * Alignement à gauche
 * @type {string} Alignement gauche
 */
const ALIGNEMENT_LEFT = 'left';
/**
 * Alignement à droite
 * @type {string} Alignement droite
 */
const ALIGNEMENT_RIGHT = 'right';

class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}