class Play extends Interface {

    constructor(refGame) {

        super(refGame, "play");

    }

    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic){

    }

}