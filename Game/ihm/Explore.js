/**
 * @classdesc IHM du mode explorer
 * @author Arnaud Kolly
 * @version 1.0
 */

class Explore extends Interface {

    /**
     * Constructeur de l'ihm du mode explorer
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "explore");
        this.refGame = refGame;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('startExplore'));


        this.showAll();
        this.init();

    }

    /**
     * Méthode métier pour tous les élements
     */
    showAll() {
        this.buttonEdu = new ButtonOnglet(430, 25, this.refGame.global.resources.getOtherText('catEducation'), COLOR_EDUCATION, COLOR_NOIR);
        var nbActiviteEdu = 3;
        var resultEdu = this.grilleVirtuelle(nbActiviteEdu);
        this.newActEdu = this.activite(resultEdu[0][0], resultEdu[0][1], 'ecole', 'Ecole');
        this.newActEdu1 = this.activite(resultEdu[1][0], resultEdu[1][1], 'prive', 'Cours prive');
        this.newActEdu2 = this.activite(resultEdu[2][0], resultEdu[2][1], 'devoirs', 'Devoirs');

        this.buttonEdu.setOnClick(function () {
            this.newActEdu.setVisible(true);
            this.newActEdu1.setVisible(true);
            this.newActEdu2.setVisible(true);
            this.newActAlim.setVisible(false);
            this.newActAlim1.setVisible(false);
            this.newActAlim2.setVisible(false);
            this.newActAlim3.setVisible(false);
            this.newActLoi.setVisible(false);
            this.newActLoi1.setVisible(false);
            this.newActLoi2.setVisible(false);
            this.newActLoi3.setVisible(false);
            this.newActLoi4.setVisible(false);
            this.newActLoi5.setVisible(false);
            this.newActLoi6.setVisible(false);
        }.bind(this));
        this.newActEdu.setOnClick(function () {
            this.popUp('ecole', this.refGame.global.resources.getOtherText('catEducation').toLowerCase());
        }.bind(this));
        this.newActEdu1.setOnClick(function () {
            this.popUp('prive', this.refGame.global.resources.getOtherText('catEducation').toLowerCase());
        }.bind(this));
        this.newActEdu2.setOnClick(function () {
            this.popUp('devoirs', this.refGame.global.resources.getOtherText('catEducation').toLowerCase());
        }.bind(this));
        this.elements.push(this.buttonEdu, this.newActEdu, this.newActEdu1, this.newActEdu2,);

        this.buttonAlim = new ButtonOnglet(290, 25, this.refGame.global.resources.getOtherText('catAlimentation'), COLOR_ALIMENTATION, COLOR_NOIR);
        var nbActiviteAlim = 4;
        var resultAlim = this.grilleVirtuelle(nbActiviteAlim);
        this.newActAlim = this.activite(resultAlim[0][0], resultAlim[0][1], 'petit-dejeuner', 'Petit-dejeuner');
        this.newActAlim1 = this.activite(resultAlim[1][0], resultAlim[1][1], 'diner', 'Diner');
        this.newActAlim2 = this.activite(resultAlim[2][0], resultAlim[2][1], 'en-cas', 'En-cas');
        this.newActAlim3 = this.activite(resultAlim[3][0], resultAlim[3][1], 'souper', 'Souper');
        this.buttonAlim.setOnClick(function () {
            this.newActAlim.setVisible(true);
            this.newActAlim1.setVisible(true);
            this.newActAlim2.setVisible(true);
            this.newActAlim3.setVisible(true);
            this.newActLoi.setVisible(false);
            this.newActLoi1.setVisible(false);
            this.newActLoi2.setVisible(false);
            this.newActLoi3.setVisible(false);
            this.newActLoi4.setVisible(false);
            this.newActLoi5.setVisible(false);
            this.newActLoi6.setVisible(false);
            this.newActEdu.setVisible(false);
            this.newActEdu1.setVisible(false);
            this.newActEdu2.setVisible(false);
        }.bind(this));
        this.newActAlim.setOnClick(function () {
            this.popUp('petit-dejeuner', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.newActAlim1.setOnClick(function () {
            this.popUp('diner', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.newActAlim2.setOnClick(function () {
            this.popUp('en-cas', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.newActAlim3.setOnClick(function () {
            this.popUp('souper', this.refGame.global.resources.getOtherText('catAlimentation').toLowerCase());
        }.bind(this));
        this.elements.push(this.buttonAlim, this.newActAlim, this.newActAlim1, this.newActAlim2, this.newActAlim3);

        this.buttonLoi = new ButtonOnglet(150, 25, this.refGame.global.resources.getOtherText('catLoisirs'), COLOR_LOISIRS, COLOR_NOIR);
        var nbActiviteLoi = 7;
        var resultLoi = this.grilleVirtuelle(nbActiviteLoi);
        this.newActLoi = this.activite(resultLoi[0][0], resultLoi[0][1], 'ecran', 'Activites sur ecran');
        this.newActLoi1 = this.activite(resultLoi[1][0], resultLoi[1][1], 'lecture', 'Lecture');
        this.newActLoi2 = this.activite(resultLoi[2][0], resultLoi[2][1], 'jeux', 'Jeux');
        this.newActLoi3 = this.activite(resultLoi[3][0], resultLoi[3][1], 'sport', 'Sport');
        this.newActLoi4 = this.activite(resultLoi[4][0], resultLoi[4][1], 'musique', 'Musique');
        this.newActLoi5 = this.activite(resultLoi[5][0], resultLoi[5][1], 'creation', 'Creation');
        this.newActLoi6 = this.activite(resultLoi[6][0], resultLoi[6][1], 'menage', 'Taches menageres');
        this.buttonLoi.setOnClick(function () {
            this.newActLoi.setVisible(true);
            this.newActLoi1.setVisible(true);
            this.newActLoi2.setVisible(true);
            this.newActLoi3.setVisible(true);
            this.newActLoi4.setVisible(true);
            this.newActLoi5.setVisible(true);
            this.newActLoi6.setVisible(true);
            this.newActAlim.setVisible(false);
            this.newActAlim1.setVisible(false);
            this.newActAlim2.setVisible(false);
            this.newActAlim3.setVisible(false);
            this.newActEdu.setVisible(false);
            this.newActEdu1.setVisible(false);
            this.newActEdu2.setVisible(false);
        }.bind(this));
        this.newActLoi.setOnClick(function () {
            this.popUp('ecran', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi1.setOnClick(function () {
            this.popUp('lecture', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi2.setOnClick(function () {
            this.popUp('jeux', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi3.setOnClick(function () {
            this.popUp('sport', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi4.setOnClick(function () {
            this.popUp('musique', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi5.setOnClick(function () {
            this.popUp('creation', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.newActLoi6.setOnClick(function () {
            this.popUp('menage', this.refGame.global.resources.getOtherText('catLoisirs').toLowerCase());
        }.bind(this));
        this.elements.push(this.buttonLoi, this.newActLoi, this.newActLoi1, this.newActLoi2, this.newActLoi3, this.newActLoi4, this.newActLoi5, this.newActLoi6);


    }

    /**
     * Méthode qui permet de créer un élement Image avec un texte en dessous.
     * @param {double} x la coordonnée x
     * @param {double} y la coordonnée y
     * @param {String} image nom de l'image
     * @param {String} text texte à afficher en dessous de l'image
     * @returns {HTMLImageElement | Image} l'élement Image avec le texte en dessous
     */
    activite(x, y, image, text) {
        this.imgActivite = this.refGame.global.resources.getImage(image).image.src;
        this.img = new Image(x, y, 100, this.imgActivite, text);
        this.img.setVisible(false);
        return this.img;
    }

    /**
     * Méthode qui permet de rechercher le texte d'une sous catégorie dans le scénario
     * @param name la sous categorie
     * @param categorie la catégorie
     * @returns {String} le texte de la sous-catégorie
     */
    jsonParseTexte(sousCat, categorie) {
        let scenAct = JSON.parse(this.refGame.global.resources.getScenario());
        let texte = scenAct[categorie][sousCat]['text'];
        return texte;
    }

    /**
     * Méthode qui permet de créer la pop-up pour chaque sous-catégorie. La pop-up contient le texte, l'image
     * et un text-to-speech pour la sous-catégorie sélectionnée
     * @param {String} sousCat la sous-catégorie
     * @param {String} categorie la catégorie
     */
    popUp(sousCat, categorie) {
        var imgSousCat = this.refGame.global.resources.getImage(sousCat).image.src;
        Swal.fire({
            title: sousCat.toUpperCase(),
            width: 1000,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px">' + '<img src=' + imgSousCat + ' width="160px" height="160px"></td>' +
                '<td><div id="activites" style="text-align: left">' + this.jsonParseTexte(sousCat, categorie) + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });

    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    refreshFont(isOpenDyslexic) {

    }

    /**
     * Cette function permet d'afficher aléatoirement les sous-catégories sur la page
     * @param nbActivite int pour le nombre d'activités
     * @returns {[]} tableau avec les résultats
     */
    grilleVirtuelle(nbActivite) {
        var curActivite = 0;
        var items = [135, 270, 405, 530];
        var grid = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
        var result = [];
        var i = 0;
        var j = 0;
        for (let k = 0; k < nbActivite; k++) {
            result[k] = new Array(0, 0);
        }
        do {
            i = Math.floor(Math.random() * 4);
            j = Math.floor(Math.random() * 4);
            if (grid[i][j] == 0) {
                result[curActivite][0] = items[i];
                result[curActivite][1] = items[j];
                curActivite++;
                grid[i][j] = 1;
            }
        } while (curActivite < nbActivite);
        return result;
    }

}