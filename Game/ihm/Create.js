class Create extends Interface {

    constructor(refGame) {

        super(refGame, "create");

    }

    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic){

    }

}